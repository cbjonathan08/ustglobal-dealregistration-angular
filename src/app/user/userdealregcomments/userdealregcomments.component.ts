import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/project';
import { AlertService, UserService, AuthenticationService } from 'src/app/_services';


@Component({
  selector: 'app-userdealregcomments',
  templateUrl: './userdealregcomments.component.html',
  styleUrls: ['./userdealregcomments.component.css']
})
export class UserdealregcommentsComponent implements OnInit {

  loading = false;
  addedComments;

  geos = [];

  projects: Project[] = [];
  newProject: Project = new Project();
  editProject: Project = new Project();
  editIndex: number = null;
  deleteProject: Project = new Project();
  deleteIndex: number = null;
  searchBy: string = "ProjectName";
  searchText: string = "";
  idOfCurrentUser;
  myDealDetails;
  error;
  DealRegID;
  code;
  commentDetails;
  private apiResponseData: any = null;

  constructor(
    private userService : UserService,
    private alertService: AlertService
  ) { }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    //console.log("id from localstorage",this.idOfCurrentUser);
  }

  loadMyDeal(){
    this.userService.loadMyDealService(this.idOfCurrentUser).subscribe((response) =>
    {
     console.log(response);
     var resultString = JSON.parse(response[0].data);
     this.myDealDetails = resultString['Table'].reverse();
     console.log(this.myDealDetails);
    },
    error => {
     console.log(error);
     this.alertService.error(error);
     this.error=error;
  }
    );

  }
  ngOnInit() {
    this.getValueFromLocalStorage();
    this.loadMyDeal();
  }

  addCommentModal(regId){
    this.DealRegID =regId;
  }

  addComment(){
    let addedCommentDetails={
      DealRegID:this.DealRegID,
      Comment:this.addedComments,
      UserId : this.idOfCurrentUser
    }
    this.userService.addCommentService(addedCommentDetails).subscribe((response) =>
    {
     console.log(response);
     var resultStatus = JSON.parse(response[0].status);
     if(resultStatus === true){
      this.alertService.success('Successfully Add');
     }
     else {
      this.alertService.error('Add fail'); 
     }
    },
    error => {
     console.log(error);
     this.alertService.error(error); 
  }
    );
  }

  viewComments(DealRegID){
    this.userService.viewCommentsService(DealRegID)
    .subscribe((response) => {
      this.apiResponseData = response;
    },
      error => {
        this.alertService.error(error);
        this.error=error;
        this.loading = false;
      }, () => {
        this.loading = false;
        if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
          var resultString = this.apiResponseData[0].data;
          var resultArray = JSON.parse(resultString);
          if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
          this.commentDetails = resultArray['Table'].reverse();

            if(this.commentDetails[0].Code === 20){
              this.code = 20;
              this.commentDetails = resultArray['Table'].reverse();
            }
            else {
              this.code = 2;
              this.commentDetails = resultArray['Table'].reverse();;
              console.log( this.commentDetails );
            
            }
          }
          else {
            this.code = 2;
            this.commentDetails = resultArray['Table'].reverse();
  
          }
        } else {
          this.alertService.error("No Response");
          this.loading = false;
        }
      })
  
}

}
