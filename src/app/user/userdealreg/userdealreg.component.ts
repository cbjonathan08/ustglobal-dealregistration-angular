import { Component, OnInit } from '@angular/core';
import { ClientService, UserService,ProjectService, TimelineService, GeoService, CountryService, AlertService, AuthenticationService,SlaService } from "./../../_services";
import { Client, Project, Geo, Country, State, TimeLine,Sla } from './../../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-userdealreg',
  templateUrl: './userdealreg.component.html',
  styleUrls: ['./userdealreg.component.css']
})

export class UserdealregComponent implements OnInit {

  
  typeOfCustomer = "New";

  clients: Client[] = [];

  countries: Country[] = [];
  states: State[] = [];
  geos: Geo[] = [];
  projects: Project[] = [];
  timeLines: TimeLine[] = [];
  sla: Sla[]=[];
  searchBy: string = "name";
  searchText: string = "";
  foundDuplicate;

  // New Entry
  clientForm: FormGroup;
  loading = false;
  submitted = false;
  display = 'none';
  error;


  idOfCurrentUser;
  keyword = 'Email';
  userEmails;
  peer='';
  constructor(
    private clientService: ClientService,
    private projectService: ProjectService,
    private timelineService: TimelineService,
    private geoService: GeoService,
    private countryService: CountryService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private slaService:SlaService,
    private userService : UserService
  ) {
  }

  ngOnInit() {
    this.getValueFromLocalStorage();
    this.getAllUsers();
    this.clientForm = this.formBuilder.group({
      name: [null, [Validators.required]],
      address: ['-'],
      contactPerson: [null, [Validators.required]],
      comments: [null],
      clientID: [''],
      projectID: ['', [Validators.required]],
      geoID: ['', [Validators.required]],
      countryID: ['', [Validators.required]],
      stateID: [''],
      timeLineID: ['', [Validators.required]],
      customerType: ['',[Validators.required]],
      slaAdmin:[null],
      slaPartner:[null],
      StateName:[null],
      peerEmails:[null]

    });

    // Grid binding
    this.clientService.getAllClients().subscribe(
      (response: Client[]) => {
        this.clients = response;
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
        // this.error = error;
      }
    );

    this.geoService.getAllGeos().subscribe(
      (response: Geo[]) => {
        this.geos = response;
      },
      (error) => {
        this.alertService.error(error);
        this.error =error;
      }
    );

    this.projectService.getAllProjects().subscribe(
      (response: Project[]) => {
        this.projects = response;
      },
      (error) => {
        this.alertService.error(error);
      }
    );

    this.timelineService.getAllTimeLines().subscribe(
      (response: TimeLine[]) => {
        this.timeLines = response;
      },
      (error) => {
        this.alertService.error(error);
      }
    );

    // Country Dropdown
    this.countryService.getAllCountries().subscribe(
      (response: Country[]) => {
        this.countries = response;
      },
      (error) => {
        this.alertService.error(error);
      }
    );

    this.slaService.getSlaData().subscribe(
        (response: Sla[]) => {
          this.sla=response;
        },
        (error) => {
          this.alertService.error(error);
        }

    );


  }

  // convenience getter for easy access to form fields
  get f() { return this.clientForm.controls; }

  getAllUsers(){
    this.userService.getAllUsersService().subscribe((response) =>
    {
     var resultString = JSON.parse(response[0].data);
     this.userEmails = resultString['Table'].reverse();

    },
    error => {
     console.log(error);
     this.alertService.error(error); 
     this.error=error;
   }
    );
  }

  selectEvent(item) {
    this.peer=item.Email;
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }
  searchCleared(){
    this.peer='';
  }
  onFocused(e) {
    // do something
  }
  onClickNewCustomer() {
    this.resetFormControls();
  }

  onClickExistingCustomer() {
    this.display = 'block';
    this.typeOfCustomer = "Existing";
    this.searchBy = "name";
    this.searchText= "";
    this.foundDuplicate = false;
  }
  onReset() {
    this.resetFormControls();
  }

  resetFormControls() {

    this.submitted = false;
    this.loading = false;
    this.states = [];
    this.clientForm.reset();
    
    // this.clientForm.patchValue({
    //   clientID: null,
    //   geoID: null,
    //   countryID: null,
    //   timeLineID: null,
    //   projectID: null,
    //   customerType: 'New'
    // });
    
    //reset
    setTimeout(() => {
      
      this.clientForm.get('clientID').reset();
      this.clientForm.get('stateID').reset();
      this.clientForm.get('countryID').reset();
      this.clientForm.get('projectID').reset();
      this.clientForm.get('geoID').reset();
      this.clientForm.get('timeLineID').reset();
      this.clientForm.get('peerEmails').reset();
      // this.clientForm.get('customerType').reset();

      this.clientForm.get('stateID').enable();
      this.clientForm.get('countryID').enable();
      this.clientForm.get('geoID').enable();
  
      this.clientForm.get('name').enable();
      this.clientForm.get('contactPerson').enable();
      this.clientForm.get('address').enable();
    });

     this.clientForm.patchValue({
      customerType: 'New'
    });
    // this.typeOfCustomer = "New";
       // this.clientForm.get('address').enable({ onlySelf: true });
      //  this.clientForm.patchValue({
      //   clientID: 0,
      // });
  }

  onSelectClick(event, index: number) {
    this.existingClientDetails(this.clients[index].countryID, index);
    this.typeOfCustomer = "Existing";
  }

  existingClientDetails(countryId: number, index: number) {

    if (countryId) {

      this.clientService.getStates(countryId).subscribe(
        (response: State[]) => {

          this.states = [];
          this.states = response;

          this.clientForm.patchValue({
            clientID: this.clients[index].clientID,
            geoID: this.clients[index].geoID,
            countryID: this.clients[index].countryID,
            stateID: this.clients[index].stateID,
            name: this.clients[index].name,
            contactPerson: this.clients[index].contactPerson,
            address: this.clients[index].address
          });

          this.clientForm.controls['geoID'].disable();
          this.clientForm.controls['countryID'].disable();
          this.clientForm.controls['stateID'].disable();

          this.clientForm.controls['name'].disable();
          this.clientForm.controls['contactPerson'].disable();
          this.clientForm.controls['address'].disable();

          // this.clientForm.get('address').disable({ onlySelf: true });

          // this.clientForm.patchValue({
          //   address: null,
          // });
          this.display = 'none';
        },
        (error) => {
          console.log(error);
          this.alertService.error(error);
        }
      );
    }
  }

  onCloseHandled() {
    // this.submitted = false;
    // this.loading = false;
    // this.states = [];
    // this.clientForm.reset();
    this.resetFormControls();
    this.display = 'none';
    // this.typeOfCustomer = "New";
    //reset
    // this.clientForm.get('stateID').reset();
    // this.clientForm.get('countryID').reset();
    // this.clientForm.get('geoID').reset();
  }

  onChangeCountry(countryId: number) {
    if (countryId) {
      this.clientService.getStates(countryId).subscribe(
        (response: State[]) => {

          this.states = [];
          this.states = response;

          //reset
          setTimeout(() => {
            this.clientForm.get('stateID').reset();
          });

        },
        (error) => {
          console.log(error);
          this.alertService.error(error);
          // this.error = error;
        }
      );
    }
  }
  
  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    console.log("id from localstorage",this.idOfCurrentUser);
  }

  onKeyUpClientName(){
    this.foundDuplicate = false;
    let result = this.clients.filter(x=>x.name.toLowerCase() === this.clientForm.get('name').value.toLowerCase());
    if(result.length !== 0){
      console.log('found duplicate')
      this.foundDuplicate = true;
    }
  }

  onSaveClick() {
   this.clientForm.value.createdBy = this.idOfCurrentUser;
   console.log(this.clientForm.value);

   if(this.clientForm.value.address === ""){
    this.clientForm.value.address = '-'
   }

   if(!this.foundDuplicate){
    this.submitted = true;
    // stop here if form is invalid
    if (this.clientForm.invalid) {
      return;
    }
    
    this.clientForm.get('address').enable({ onlySelf: true });
    let sladetails=this.sla;
    for (var sladetail of sladetails) {
      if(sladetail.roleID==1){
        this.clientForm.value.slaAdmin=sladetail.slatime;   
      }
      if(sladetail.roleID==34){
        this.clientForm.value.slaPartner=sladetail.slatime;   
      }
    }
    this.clientForm.value.peerEmails=this.peer;   

    // // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.clientForm.value, null, 4));
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.clientForm.getRawValue(), null, 4));

    this.loading = true;
    this.clientService.dealregistration(this.clientForm.value)
        .pipe(first())
        .subscribe(
            data => {
              //debugger;
                this.alertService.success('New Deal Registration successful', true);
                this.submitted = false;
                this.loading = false;
                this.clientForm.reset();
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            });
   }

   else{
    console.log("found duplicates");
    this.alertService.error('Client name already exist !')
   }
   

  }
}
