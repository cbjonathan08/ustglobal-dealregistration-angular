import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/project';
import { AlertService, UserService, AuthenticationService } from 'src/app/_services';
  
@Component({
  selector: 'app-userdealregstatus',
  templateUrl: './userdealregstatus.component.html',
  styleUrls: ['./userdealregstatus.component.css']
})
export class UserdealregstatusComponent implements OnInit {

  
  loading = false;

  geos = [ 
    { DealRegID:101, Comments: 'No Comments', MemberName: 'Patan Dadapeer Khan', Geo:'ASIA', Country:'India', State:'Telangana', TimeLine:'One Month', DateOfReg:'21/01/2020', ClientName:'Intel Technology Sdn. Bhd.', ClientAddress:'Penang, Malaysia', ContactPerson:'Nittin Dut', Status:'Rejected',   CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020' },
    { DealRegID:102, Comments: '---', MemberName: 'Patan Dadapeer Khan', Geo:'ASIA', Country:'India', State:'Telangana', TimeLine:'One Month', DateOfReg:'21/01/2020', ClientName:'UST Global (Malaysia) Sdn Bhd', ClientAddress:'Penang, Malaysia', ContactPerson:'Nittin Dut', Status:'Approved',   CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020' },
    { DealRegID:103, Comments: 'Compensation letter for my assignment in India', MemberName: 'Patan Dadapeer Khan', Geo:'ASIA', Country:'India', State:'Telangana', TimeLine:'One Month', DateOfReg:'21/01/2020', ClientName:'HRK SOFTWARE Solutions Pvt Ltd', ClientAddress:'Hyderabad, India', ContactPerson:'Nittin Dut', Status:'Pending',   CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020' },
  
  ];

  projects: Project[] = [];
  newProject: Project = new Project();
  editProject: Project = new Project();
  editIndex: number = null;
  deleteProject: Project = new Project();
  deleteIndex: number = null;
  searchBy: string = "client";
  searchText: string = "";
  idOfCurrentUser;
  myDealDetails;
  error;
  editedComments;
  private apiResponseData: any = null;

  constructor(
    private userService : UserService,
    private alertService: AlertService
  ) { }

  editCommentId;
  getCommentId(event) {
    this.editedComments = "";
    this.editCommentId = event;
  }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    //console.log("id from localstorage",this.idOfCurrentUser);
  }

  getUserRegistrationDeal(){
    this.userService.getUserRegistrationDealService(this.idOfCurrentUser).subscribe((response) =>
    {
     console.log(response);
     var resultString = JSON.parse(response[0].data);
     this.myDealDetails = resultString['Table'].reverse();
     console.log(this.myDealDetails);
    },
    error => {
     console.log(error);
     this.alertService.error(error);
     this.error=error;
  }
    );

  }
  ngOnInit() {
    this.getValueFromLocalStorage();
    this.getUserRegistrationDeal();
  }

  updateComment() {

    let statusDetail = {
      DealId: this.editCommentId,
      DealComment: this.editedComments
    }
    if ((this.editCommentId).toString().trim() != "" && (this.editedComments).toString().trim() != "") {
      this.loading = true;
      this.apiResponseData = null;
      this.userService.updateCommentService(statusDetail)
        .subscribe((response) => {
          console.log(response);
          this.apiResponseData = response;
          this.getUserRegistrationDeal();
        },
          error => {
            this.alertService.error(error);
            this.loading = false;
          }, () => {
            this.loading = false;
            if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
              var resultString = this.apiResponseData[0].data;
              var resultArray = JSON.parse(resultString);
              if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
                this.alertService.success("Updated Successfully !");
                this.geos = resultArray['Table'];
              }
            } else {
              this.alertService.error("No Response");
              this.loading = false;
            }
          });
    } else {
      this.alertService.error("Please select deal and enter comments");
      this.loading = false;
    }
  }


}