import { Component, OnInit } from '@angular/core';
import { AlertService, UserService, AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-usermanageprofile',
  templateUrl: './usermanageprofile.component.html',
  styleUrls: ['./usermanageprofile.component.css']
})
export class UsermanageprofileComponent implements OnInit {

  loading = false;
  edited = {};
  idOfCurrentUser;
  error;
  user;



  constructor(
    private userService : UserService,
    private alertService: AlertService
  ) { }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
  }

  ngOnInit() {
    this.getValueFromLocalStorage();
    this.getSingleUser();
  }

  getSingleUser(){
    this.userService.getSingleUserService(this.idOfCurrentUser).subscribe((response) =>
    {
     console.log(response);
     var resultString = JSON.parse(response[0].data);
     var resultStatus = JSON.parse(response[0].status);
     this.user = resultString['Table'];
     if(resultStatus === false){
      this.alertService.error('Something went wrong.Please try again later.'); 
     }
     
    },
    error => {
     console.log(error);
     this.alertService.error(error); 
     this.error=error;
  }
    );

  }

  editUser(details){
    //onsole.log("edit user clicked");
    //console.log(details);
    this.edited=details;
    console.log(this.edited);
  }

  updateProfile(){
    this.userService.updateProfileService(this.edited).subscribe((response) =>
    {
     console.log(response);
     var resultStatus = JSON.parse(response[0].status);
     if(resultStatus === true){
      this.alertService.success('Successfully updated');
     }
     else {
      this.alertService.error('Update fail'); 
     }
    },
    error => {
     console.log(error);
     this.alertService.error(error); 
  }
    );

  }


}
