import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsermanageprofileComponent } from './usermanageprofile.component';

describe('UsermanageprofileComponent', () => {
  let component: UsermanageprofileComponent;
  let fixture: ComponentFixture<UsermanageprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsermanageprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsermanageprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
