import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/project';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService, UserService, AlertService,SlaService } from 'src/app/_services';
import { Sla } from 'src/app/_models';

import { Router } from '@angular/router';

@Component({
  selector: 'app-partnerdealstatus',
  templateUrl: './partnerdealstatus.component.html',
  styleUrls: ['./partnerdealstatus.component.css']
})
export class PartnerdealstatusComponent implements OnInit {

  loading = false;
  geos = [];

  // geos = [ 
  //   { DealRegID:101, MemberName: 'Patan Dadapeer Khan', Geo:'ASIA', Country:'India', State:'Telangana', TimeLine:'One Month', DateOfReg:'21/01/2020', ClientName:'Intel Technology Sdn. Bhd.', ClientAddress:'Penang, Malaysia', ContactPerson:'Nittin Dut', Status:'Rejected', Comments:'---',  CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020' },
  //   { DealRegID:102, MemberName: 'Patan Dadapeer Khan', Geo:'ASIA', Country:'India', State:'Telangana', TimeLine:'One Month', DateOfReg:'21/01/2020', ClientName:'UST Global (Malaysia) Sdn Bhd', ClientAddress:'Penang, Malaysia', ContactPerson:'Nittin Dut', Status:'Approved', Comments:'---',  CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020' },
  //   { DealRegID:103, MemberName: 'Patan Dadapeer Khan', Geo:'ASIA', Country:'India', State:'Telangana', TimeLine:'One Month', DateOfReg:'21/01/2020', ClientName:'HRK SOFTWARE Solutions Pvt Ltd', ClientAddress:'Hyderabad, India', ContactPerson:'Nittin Dut', Status:'Pending', Comments:'---',  CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020' },

  // ];

  projects: Project[] = [];
  newProject: Project = new Project();
  editProject: Project = new Project();
  editIndex: number = null;
  deleteProject: Project = new Project();
  deleteIndex: number = null;
  searchBy: string = "ProjectName";
  searchText: string = "";
  error;
  adminComments;
  adminRejectComments;
  selectedAll: any;
  changeStatus = [];
  createdBy;
  modifiedBy;
  idOfCurrentUser;
  adminSla;
  partnerSla;
  sla: Sla[]=[];
  commentForm: FormGroup;
  commentRejectionForm: FormGroup;
  submitted = false;
  display = 'none';
  rejectDisplay='none';
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService,
    private slaService:SlaService
  ) {
    // redirect to home if already logged in
    // if (this.authenticationService.currentUserValue) {
    //   //debugger;
    //   this.router.navigate(['/']);
    // }
  }

  ngOnInit() {
    this.commentForm = this.formBuilder.group({
      adminComments:[null, [Validators.required]]
    });
    this.commentRejectionForm  = this.formBuilder.group({
      adminRejectComments:[null, [Validators.required]]
    });
    // this.userService.adminDealStatusService().subscribe((response) => {
    //   this.geos = response;
    //   for (var i = 0; i < this.geos.length; i++) {
    //     this.geos[i].selected = false;
    //   }

    // },
    //   error => {
    //     this.alertService.error(error);
    //     this.loading = false;
    //   })
    this.getAllDealRegInfo();
    this.getValueFromLocalStorage();

  }
  get f() { return this.commentForm.controls; }
  get fr() { return this.commentRejectionForm.controls; }
  openModal() {
    this.display = 'block';
  }
  openRejectModal(){
    this.rejectDisplay = 'block';
  }

  onCloseHandled() {
    this.submitted = false;
    this.loading = false;
    this.commentForm.controls['adminComments'].reset();
    this.commentRejectionForm.controls['adminRejectComments'].reset();
    this.display = 'none';
    this.rejectDisplay = 'none';
  }
  checkStatus(event) {
    var index = this.changeStatus.indexOf(event.target.value);
 
    if (index === -1) {
      // val not found, pushing onto array
      this.changeStatus.push(event.target.value);
    }
    else {
      // val is found, removing from array
      this.changeStatus.splice(index, 1);
    }

  }
  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
  }
  finalValue = [];
  private apiResponseData: any = null;
  getAllDealRegInfo() {
    this.loading = true;
    this.apiResponseData = null;
  
    this.userService.GetDealRegInfoPartner_ADO()
      .subscribe((response) => {
        console.log(response);
        this.apiResponseData = response;
      },
        error => {
          this.alertService.error(error);
          this.loading = false;
          this.error=error;
        }, () => {
          this.loading = false;
          if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
            var resultString = this.apiResponseData[0].data;
            var resultArray = JSON.parse(resultString);
            if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
              this.geos = resultArray['Table'].reverse();
              console.log(this.geos);
              for (var i = 0; i < this.geos.length; i++) {
                this.geos[i].selected = false;
              }
            }
          } else {
            this.alertService.error("No Response");
            this.loading = false;
            
          }
        })
  }
  updateComment(stype:string){
    let arr=this.changeStatus;
    for (var val of arr) {
      let delregComments = {
        DealRegID: val, //["32","33"]
        Comments:(stype == 'approve' ? this.adminComments:this.adminRejectComments),
        UserId:this.idOfCurrentUser
      }
      this.userService.addCommentService(delregComments).subscribe(
      (response) => {
        this.alertService.success("Updated Successfully !");
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
        // this.error = error;
        // this.loading = false;
      }
    );
    }
    this.onCloseHandled();
  }
  approveOrRejectStatus(stype:string) {
    this.submitted = true;
    if (this.commentForm.invalid && stype == 'approve') {
      return;
    }
    if (this.commentRejectionForm.invalid && stype == 'reject') {
      return;
    }
    this.display = 'none';
    this.rejectDisplay = 'none';
    let statusDetails = {
        DealIds: this.changeStatus.join(','), //["32","33"]
        Status: (stype == 'approve' ? 2: 3),
        Comments:(stype == 'approve' ? this.adminComments:this.adminRejectComments)
    }
    
    if (this.changeStatus.length > 0) {
      this.loading = true;
      this.apiResponseData = null;
      this.userService.approveOrRejectStatusServicePartner(statusDetails)
        .subscribe((response) => {
          console.log(response);
          this.apiResponseData = response;
          this.updateComment(stype);
        },
          error => {
            this.alertService.error("Seleced Deals are already approved OR rejected please select only pending ones");
            this.loading = false;
          }, () => {
            this.loading = false;
            this.changeStatus = [];
            this.selectedAll = false;
            if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
              var resultString = this.apiResponseData[0].data;
              var resultArray = JSON.parse(resultString);
              if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
                this.alertService.success("Updated Successfully !");
                this.geos = resultArray['Table'].reverse();
                for (var i = 0; i < this.geos.length; i++) {
                  this.geos[i].selected = false;
                }
              }
            } else {
              this.alertService.error("No Response");
              this.loading = false;
            }
          });
    }else {
      this.alertService.error("Please select deals");
      this.loading = false;
    }
  }
  CheckAllOptions() {
    //this.selectedAll = !this.selectedAll;
    if (!this.selectedAll) {
      this.selectedAll = true;

      for (var i = 0; i < this.geos.length; i++) {
        this.geos[i].selected = this.selectedAll;

        this.checkStatus({ target: { value: this.geos[i].DealRegID.toString() } })
      }


    }
    else if (this.selectedAll) {
      this.selectedAll = false;
      for (var i = 0; i < this.geos.length; i++) {
        this.geos[i].selected = this.selectedAll;

        this.checkStatus({ target: { value: this.geos[i].DealRegID.toString() } })
      }

    }


  }
}
