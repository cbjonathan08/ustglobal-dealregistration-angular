export class Client
{
    clientID: number;
    name: string;
    address: string;
    contactPerson: string;
    geoID: number;
    countryID: number;
    stateID: number;
    createdBy: number;
    createdOn: string;
    modifiedBy: number;
    modifiedOn: string;
    UserID:number;

    constructor()
    {
        
        this.clientID = null;
        this.name = null;
        this.address = null;
        this.contactPerson = null;
        this.geoID = null;
        this.countryID = null;
        this.stateID = null;
        this.createdBy = null;
        this.createdOn = null;
        this.modifiedBy = null;
        this.modifiedOn = null;
        this.UserID = null;
    }
}
