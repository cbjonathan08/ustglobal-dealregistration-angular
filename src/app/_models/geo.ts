export class Geo
{
    GeoID: number;
    GeoName: string;
    CreatedBy: number;
    createdOn: string;
    modifiedBy: number;
    modifiedOn: string;

    constructor()
    {
        this.GeoID = null;
        this.GeoName = null;
        this.CreatedBy = null;
        this.createdOn = null;
        this.modifiedBy = null;
        this.modifiedOn = null;
    }
}
