import { Role } from "./Role";

export class MemberRegistration {
    UserID: number;
    RoleID: number;
    FirstName: string;
    LastName: string;
    Email: string;
    Password: string;  
    PhoneNumber: string;
    IsActive: boolean;
    CreatedBy: number;
    CreatedOn: Date;
    ModifiedBy: number;
    ModifiedOn: Date;
    token?: string;
    role: Role;
  }