import { Role } from "./role";

export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    memberType?: number;
    contactNumber?: number;
    created_on?:any;
    role: Role;
    token?: string;
}