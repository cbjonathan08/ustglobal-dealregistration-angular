export class Project
{
    projectID: number;
    projectName: string;
    createdBy: number;
    createdOn: string;
    modifiedBy: number;
    modifiedOn: string;

    constructor()
    {
        this.projectID = null;
        this.projectName = null;
        this.createdBy = null;
        this.createdOn = null;
        this.modifiedBy = null;
        this.modifiedOn = null;
    }
}
