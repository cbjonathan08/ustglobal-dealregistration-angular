export class Sla
{
    slaID: number;
    roleID: number;
    slatime: number;
    createdBy: number;
    createdOn: string;
    modifiedBy: number;
    modifiedOn: string;

    constructor()
    {
        this.slaID = null;
        this.roleID = null;
        this.slatime = null;
        this.createdBy = null;
        this.createdOn = null;
        this.modifiedBy = null;
        this.modifiedOn = null;
    }
}
