export class Roles
{
    roleID: number;
    roleName: string;
    roleDesc: string;
    createdBy: number;
    createdOn: string;
    modifiedBy: number;
    modifiedOn: string;

    constructor()
    {
        this.roleID = null;
        this.roleName = null;
        this.roleDesc = null;
        this.createdBy = null;
        this.createdOn = null;
        this.modifiedBy = null;
        this.modifiedOn = null;
    }
}
