export class Country
{
    countryID: number;
    countryName: string;
    createdBy: number;
    createdOn: string;
    modifiedBy: number;
    modifiedOn: string;

    constructor()
    {
        this.countryID = null;
        this.countryName = null;
        this.createdBy = null;
        this.createdOn = null;
        this.modifiedBy = null;
        this.modifiedOn = null;
    }
}
