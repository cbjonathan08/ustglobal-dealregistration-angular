export class State
{
    stateID: number;
    countryID: number;
    stateName: string;
    countryName: string;
    createdBy: number;
    createdOn: string;
    modifiedBy: number;
    modifiedOn: string;

    constructor()
    {
        this.stateID = null;
        this.countryID = null;
        this.stateName = null;
        this.countryName = null;
        this.createdBy = null;
        this.createdOn = null;
        this.modifiedBy = null;
        this.modifiedOn = null;
    }
}
