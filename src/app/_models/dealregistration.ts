export class DealRegistration
{
    clientID: number;
    ClientName: string;
    address: string;
    contactPerson: string;
    geoID: number;
    countryID: number;
    stateID: number;
    timeLineID: number;
    projectID : number;
    slaAdmin:number;
    slaPartner:number;
    comments : string;
    createdBy: number;
    createdOn: string;
    modifiedBy: number;
    modifiedOn: string;
    regType: string;

    constructor()
    {
        this.clientID = null;
        this.ClientName = null;
        this.address = null;
        this.contactPerson = null;
        this.geoID = null;
        this.countryID = null;
        this.stateID = null;
        this.timeLineID = null;
        this.projectID = null;
        this.slaAdmin=null;
        this.slaPartner=null;
        this.createdBy = null;
        this.createdOn = null;
        this.modifiedBy = null;
        this.modifiedOn = null;
        this.regType = null;
    }
}
