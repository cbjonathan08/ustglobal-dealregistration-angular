import { Component, OnInit } from '@angular/core';
import { ClientService, GeoService, CountryService, AlertService, AuthenticationService } from "./../../_services";
import { Client, Geo, Country, State } from './../../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  clients: Client[] = [];
  countries: Country[] = [];
  states: State[] = [];
  geos: Geo[] = [];

  // New Entry
  clientForm: FormGroup;
  loading = false;
  submitted = false;
  display = 'none';
  createdBy;
  modifiedBy;

  // Edit Entry
  editClientForm: FormGroup;
  editLoading = false;
  editSubmitted = false;
  editDisplay = 'none';
  editIndex: number = null;

  // Delete Entry
  deleteClientForm: FormGroup;
  deleteLoading = false;
  deleteSubmitted = false;
  deleteDisplay = 'none';
  deleteIndex: number = null;
  error;;
  

  idOfCurrentUser;

  constructor(
    private clientService: ClientService,
    private geoService: GeoService,
    private countryService: CountryService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
  }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    console.log("id from localstorage",this.idOfCurrentUser);
  }

  ngOnInit() {

    this.getValueFromLocalStorage();

    this.clientForm = this.formBuilder.group({
      name: [null, [Validators.required]],
      address: ['-'],
      contactPerson: [null, [Validators.required]],
      geoID: ['', [Validators.required]],
      countryID: ['', [Validators.required]],
      stateID: [''],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.editClientForm = this.formBuilder.group({
      clientID: [null, [Validators.required]],
      geoID: ['', [Validators.required]],
      countryID: [null, [Validators.required]],
      stateID: ['', [Validators.required]],
      name: [null, [Validators.required]],
      address: [null, [Validators.required]],
      contactPerson: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.deleteClientForm = this.formBuilder.group({
      clientID: [null, [Validators.required]],
      countryID: [null, [Validators.required]],
      name: [null, [Validators.required]]
    });

    //geo dropdown
    this.geoService.getAllGeos().subscribe(
      (response: Geo[]) => {
        this.geos = response;
      },
      (error) => {
        this.alertService.error(error);
        // this.error = error;
      }
    );
    
    // Country Dropdown
    this.countryService.getAllCountries().subscribe(
      (response: Country[]) => {
        this.countries = response;
      },
      (error) => {
        this.alertService.error(error);
      }
    );

    let clientDetails= {
      UserId:this.idOfCurrentUser,
      Type:'Client'
    }

    // Grid binding
    this.getAllClient();
  }

  onChangeCountry(countryId: number) {

    if (countryId) {
     
      this.clientService.getStates(countryId).subscribe(
        (response: State[]) => {
       
          this.states = [];
          this.states = response;

          //reset
          setTimeout(() => {
            this.clientForm.get('stateID').reset();
          });
          
        },
        (error) => {
          console.log(error);
          this.alertService.error(error);
          // this.error = error;
        }
      );
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.clientForm.controls; }
  get fedit() { return this.editClientForm.controls; }
  get fdelete() { return this.deleteClientForm.controls; }

  openModal() {
    this.display = 'block';
  }

  onCloseHandled() {
    this.submitted = false;
    this.loading = false;
    this.states = [];
    this.clientForm.reset();
    this.display = 'none';

    //reset
    setTimeout(() => {
      this.clientForm.get('stateID').reset();
      this.clientForm.get('countryID').reset();
      this.clientForm.get('geoID').reset();
    });
  }

  onEditCloseHandled() {
    this.editSubmitted = false;
    this.editLoading = false;
    this.editClientForm.reset();
    this.editDisplay = 'none';
  }

  onDeleteCloseHandled() {
    this.deleteSubmitted = false;
    this.deleteLoading = false;
    this.deleteClientForm.reset();
    this.deleteDisplay = 'none';
  }

  getAllClient(){
    let clientDetails= {
      UserId:this.idOfCurrentUser,
      Type:'Client'
    }
    this.clientService.getAllClientService(clientDetails).subscribe(
      (response) => {
        var resultString = JSON.parse(response[0].data);
        this.clients = resultString['Table'].reverse();
        console.log(this.clients);
       
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
        // this.error = error;
        // this.loading = false;
      }
    );
  }

  onSaveClick() {
    console.log(this.clientForm.value.address);
    if(this.clientForm.value.address === "" || this.clientForm.value.address === null){
      this.clientForm.value.address = '-'
     }
  
    this.submitted = true;

    // stop here if form is invalid
    if (this.clientForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.clientForm.value, null, 4));

    this.loading = true;
    this.clientForm.value.createdBy = this.idOfCurrentUser;
    this.clientService.insert(this.clientForm.value)
      .pipe(first())
      .subscribe(
        (response) => {
        
          //Add Geo to Grid
          var p: Client = new Client();
          p.clientID = response.clientID;
          p.name = response.name;
          p.address = response.address;
          p.contactPerson = response.contactPerson;
          p.geoID = response.geoID;
          p.countryID = response.countryID;
          p.stateID = response.stateID;

          p.createdBy = response.createdBy;
          p.createdOn = response.createdOn;
          p.modifiedBy = response.modifiedBy;
          p.modifiedOn = response.modifiedOn;
          this.clients.push(p);

          this.alertService.success('Client created successfully!', true);
          // this.error ="Registration successful";
          this.onCloseHandled();
          this.getAllClient();


        }, (error) => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onEditClick(event, index: number) {
    console.log(event);
    this.editDisplay = 'block';
    this.editIndex = index;
    this.onEditChangeCountry(this.clients[index].countryID, index);
  }

  onEditChangeCountry(countryId: number, index: number) {

    if (countryId) {
     
      this.clientService.getStates(countryId).subscribe(
        (response: State[]) => {
       
          this.states = [];
          this.states = response;
          console.log(this.clients[index]);

          this.editClientForm.setValue({
            clientID: this.clients[index].clientID,
            geoID: this.clients[index].geoID,
            countryID: this.clients[index].countryID,
            stateID: this.clients[index].stateID,
            name: this.clients[index].name,
            contactPerson: this.clients[index].contactPerson,
            address: this.clients[index].address,
            createdBy:this.clients[index].UserID,
            modifiedBy:this.idOfCurrentUser
          });
          
        },
        (error) => {
          console.log(error);
          this.alertService.error(error);
          // this.error = error;
        }
      );
    }
  }

  onUpdateClick() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editClientForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.editClientForm.value, null, 4));
    debugger;
    this.editLoading = true;

    this.clientService.update(this.editClientForm.value)
      .pipe(first())
      .subscribe((response: Client) => {

        var p: Client = new Client();
        p.clientID = response.clientID;
        p.name = response.name;
        p.address = response.address;
        p.contactPerson = response.contactPerson;
        p.geoID = response.geoID;
        p.countryID = response.countryID;
        p.stateID = response.stateID;

        p.createdBy = response.createdBy;
        p.createdOn = response.createdOn;
        p.modifiedBy = response.modifiedBy;
        p.modifiedOn = response.modifiedOn;
        this.clients[this.editIndex] = p;

        this.alertService.success('Client updated successfully!', true);
        // this.error ="Registration successful";
        this.onEditCloseHandled();
        this.getAllClient();

      },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });

  }

  onDeleteClick(event, index: number) {

    this.deleteDisplay = 'block';
    this.deleteIndex = index;

    this.deleteClientForm.setValue({
      clientID: this.clients[index].clientID,
      countryID: this.clients[index].countryID,
      name: this.clients[index].name
    });
  }

  onDeleteConfirmClick() {

    this.deleteSubmitted = true;

    // stop here if form is invalid
    if (this.deleteClientForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.deleteClientForm.value, null, 4));
    debugger;
    this.deleteLoading = true;

    this.clientService.delete(this.deleteClientForm.value.clientID)
      .pipe(first())
      .subscribe(
        (response) => {
          debugger;
          this.clients.splice(this.deleteIndex, 1);

          this.alertService.success('Client deleted successfully!', true);
          // this.error ="Registration successful";
          this.onDeleteCloseHandled();

        },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });
  }
}
