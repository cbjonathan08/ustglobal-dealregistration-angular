import { Component, OnInit } from '@angular/core';
import { GeoService, AlertService, AuthenticationService } from "./../../_services";
import { Geo } from './../../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-geo',
  templateUrl: './geo.component.html',
  styleUrls: ['./geo.component.css']
})
export class GeoComponent implements OnInit {

  geos: Geo[] = [];

  // New Entry
  geoForm: FormGroup;
  loading = false;
  submitted = false;
  display = 'none';

  // Edit Entry
  editGeoForm: FormGroup;
  editLoading = false;
  editSubmitted = false;
  editDisplay = 'none';
  editIndex: number = null;

  // Delete Entry
  deleteGeoForm: FormGroup;
  deleteLoading = false;
  deleteSubmitted = false;
  deleteDisplay = 'none';
  deleteIndex: number = null;
  temp;

  idOfCurrentUser;
  error;
  createdBy;
  modifiedBy;

  constructor(
    private geoService: GeoService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
  }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    console.log("id from localstorage",this.idOfCurrentUser);
  }

  ngOnInit() {
    this.getValueFromLocalStorage();

    this.geoForm = this.formBuilder.group({
      GeoName: [null, [Validators.required]]
    });

    this.editGeoForm = this.formBuilder.group({
      GeoID: [null, [Validators.required]],
      GeoName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.deleteGeoForm = this.formBuilder.group({
      GeoID: [null, [Validators.required]],
      GeoName: [null, [Validators.required]]
    });

    this.getAllGeo();
   
 
 
  }

 
   // Grid binding
  getAllGeo(){
    let geoDetails= {
      UserId:this.idOfCurrentUser,
      Type:'Geo'
    };
    this.geoService.getAllGeoService(geoDetails).subscribe(
      (response) => {
        var resultString = JSON.parse(response[0].data);
        this.temp = resultString['Table'];
        this.geos=this.temp.reverse();
        console.log(this.geos);
       
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
        // this.error = error;
        // this.loading = false;
      })
  }

  // convenience getter for easy access to form fields
  get f() { return this.geoForm.controls; }
  get fedit() { return this.editGeoForm.controls; }
  get fdelete() { return this.deleteGeoForm.controls; }

  openModal() {
    this.display = 'block';
  }

  onCloseHandled() {
    this.submitted = false;
    this.loading = false;
    this.geoForm.reset();
    this.display = 'none';
  }

  onEditCloseHandled() {
    this.editSubmitted = false;
    this.editLoading = false;
    this.editGeoForm.reset();
    this.editDisplay = 'none';
  }

  onDeleteCloseHandled() {
    this.deleteSubmitted = false;
    this.deleteLoading = false;
    this.deleteGeoForm.reset();
    this.deleteDisplay = 'none';
  }


  onSaveClick() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.geoForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.geoForm.value, null, 4));

    this.loading = true;
    this.geoForm.value.createdBy = this.idOfCurrentUser;
    this.geoService.insertGeo(this.geoForm.value)
      .pipe(first())
      .subscribe(
        (response) => {
         

          //Add Geo to Grid
          var p: Geo = new Geo();
          p.GeoID = response.GeoID;
          p.GeoName = response.GeoName;
          p.CreatedBy = response.CreatedBy;
          p.createdOn = response.createdOn;
          p.modifiedBy = response.modifiedBy;
          p.modifiedOn = response.modifiedOn;
          this.geos.push(p);

          this.getAllGeo()

          this.alertService.success('Geo created successfully!', true);
          // this.error ="Registration successful";
          this.onCloseHandled();


        }, (error) => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onEditClick(event, index: number) {
    console.log(event);
    this.editDisplay = 'block';
    this.editIndex = index;

    this.editGeoForm.setValue({
      GeoID: this.geos[index].GeoID,
      GeoName: this.geos[index].GeoName,
      // CreatedBy: this.geos[index].CreatedBy,
      createdBy:event.UserID,
      modifiedBy:this.idOfCurrentUser
    });

  }

  onUpdateClick() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editGeoForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.editGeoForm.value, null, 4));

    this.editLoading = true;

    this.geoService.updateGeo(this.editGeoForm.value)
      .pipe(first())
      .subscribe((response: Geo) => {

        this.alertService.success('Geo updated successfully!', true);
        // this.error ="Registration successful";
        this.onEditCloseHandled();
        this.getAllGeo();

      },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });

  }

  onDeleteClick(event, index: number) {

    this.deleteDisplay = 'block';
    this.deleteIndex = index;

    this.deleteGeoForm.setValue({
      GeoID: this.geos[index].GeoID,
      GeoName: this.geos[index].GeoName
    });
  }

  onDeleteConfirmClick() {

    this.deleteSubmitted = true;

    // stop here if form is invalid
    if (this.deleteGeoForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.deleteGeoForm.value, null, 4));
    debugger;
    this.deleteLoading = true;

    this.geoService.deleteGeo(this.deleteGeoForm.value.GeoID)
      .pipe(first())
      .subscribe(
        (response) => {
          debugger;
          this.geos.splice(this.deleteIndex, 1);

          this.alertService.success('Geo deleted successfully!', true);
          // this.error ="Registration successful";
          this.onDeleteCloseHandled();

        },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });
  }

}
