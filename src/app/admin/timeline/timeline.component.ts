import { Component, OnInit } from '@angular/core';
import { TimelineService, AlertService, AuthenticationService } from "./../../_services";
import { TimeLine } from './../../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  timeLines: TimeLine[] = [];

  // New Entry
  timeLineForm: FormGroup;
  loading = false;
  submitted = false;
  display = 'none';
  createdBy;
  modifiedBy;

  // Edit Entry
  editTimeLineForm: FormGroup;
  editLoading = false;
  editSubmitted = false;
  editDisplay = 'none';
  editIndex: number = null;

  // Delete Entry
  deleteTimeLineForm: FormGroup;
  deleteLoading = false;
  deleteSubmitted = false;
  deleteDisplay = 'none';
  deleteIndex: number = null;

  idOfCurrentUser;
  error;

  constructor(
    private timeLineService: TimelineService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
  }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    console.log("id from localstorage",this.idOfCurrentUser);
  }

  ngOnInit() {

    this.getValueFromLocalStorage();

    this.timeLineForm = this.formBuilder.group({
      timeLineName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.editTimeLineForm = this.formBuilder.group({
      timeLineID: [null, [Validators.required]],
      timeLineName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.deleteTimeLineForm = this.formBuilder.group({
      timeLineID: [null, [Validators.required]],
      timeLineName: [null, [Validators.required]]
    });

    // Grid binding
    this.getAllTimeLine();
  }

  getAllTimeLine(){
    let timeLineDetails= {
      UserId:this.idOfCurrentUser,
      Type:'TimeLine'
    }
    this.timeLineService.getAllTimeLineService(timeLineDetails).subscribe(
      (response) => {
        var resultString = JSON.parse(response[0].data);
        this.timeLines = resultString['Table'].reverse();
        console.log(this.timeLines);
       
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
        // this.error = error;
        // this.loading = false;
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() { return this.timeLineForm.controls; }
  get fedit() { return this.editTimeLineForm.controls; }
  get fdelete() { return this.deleteTimeLineForm.controls; }

  openModal() {
    this.display = 'block';
  }

  onCloseHandled() {
    this.submitted = false;
    this.loading = false;
    this.timeLineForm.reset();
    this.display = 'none';
  }

  onEditCloseHandled() {
    this.editSubmitted = false;
    this.editLoading = false;
    this.editTimeLineForm.reset();
    this.editDisplay = 'none';
  }

  onDeleteCloseHandled() {
    this.deleteSubmitted = false;
    this.deleteLoading = false;
    this.deleteTimeLineForm.reset();
    this.deleteDisplay = 'none';
  }


  onSaveClick() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.timeLineForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.timeLineForm.value, null, 4));

    this.loading = true;
    this.timeLineForm.value.createdBy = this.idOfCurrentUser;
    this.timeLineService.insertTimeLine(this.timeLineForm.value)
      .pipe(first())
      .subscribe(
        (response) => {
         
          //Add Geo to Grid
          var p: TimeLine = new TimeLine();
          p.timeLineID = response.timeLineID;
          p.timeLineName = response.timeLineName;
          p.createdBy = response.createdBy;
          p.createdOn = response.createdOn;
          p.modifiedBy = response.modifiedBy;
          p.modifiedOn = response.modifiedOn;
          this.timeLines.push(p);

          this.alertService.success('Time Line created successfully!', true);
          // this.error ="Registration successful";
          this.onCloseHandled();
          this.getAllTimeLine();


        }, (error) => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onEditClick(event, index: number) {

    this.editDisplay = 'block';
    this.editIndex = index;

    this.editTimeLineForm.setValue({
      timeLineID: this.timeLines[index].timeLineID,
      timeLineName: this.timeLines[index].timeLineName,
      createdBy:event.UserID,
      modifiedBy:this.idOfCurrentUser
    });

  }

  onUpdateClick() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editTimeLineForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.editTimeLineForm.value, null, 4));
  
    this.editLoading = true;

    this.timeLineService.updateTimeLine(this.editTimeLineForm.value)
      .pipe(first())
      .subscribe((response: TimeLine) => {

 

        this.alertService.success('Time Line updated successfully!', true);
        // this.error ="Registration successful";
        this.onEditCloseHandled();
        this.getAllTimeLine();

      },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });

  }

  onDeleteClick(event, index: number) {
    console.log(this.deleteTimeLineForm);
    this.deleteDisplay = 'block';
    this.deleteIndex = index;

    this.deleteTimeLineForm.setValue({
      timeLineID: this.timeLines[index].timeLineID,
      timeLineName: this.timeLines[index].timeLineName
    });
  }

  onDeleteConfirmClick() {

    this.deleteSubmitted = true;

    // stop here if form is invalid
    if (this.deleteTimeLineForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.deleteTimeLineForm.value, null, 4));
    
    this.deleteLoading = true;

    this.timeLineService.deleteTimeLine(this.deleteTimeLineForm.value.timeLineID)
      .pipe(first())
      .subscribe(
        (response) => {
        
          this.timeLines.splice(this.deleteIndex, 1);

          this.alertService.success('Time Line deleted successfully!', true);
          // this.error ="Registration successful";
          this.onDeleteCloseHandled();

        },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });
  }

}
