import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/project';
import { AuthenticationService, UserService, AlertService } from 'src/app/_services';

@Component({
  selector: 'app-dealcomments',
  templateUrl: './dealcomments.component.html',
  styleUrls: ['./dealcomments.component.css']
})
export class DealcommentsComponent implements OnInit {

  loading = false;

  geos = [];

  projects: Project[] = [];
  newProject: Project = new Project();
  editProject: Project = new Project();
  editIndex: number = null;
  deleteProject: Project = new Project();
  deleteIndex: number = null;
  searchBy: string = "ProjectName";
  searchText: string = "";
  editedComments;
  error;
  commentDetails;
  commentNotFound;
  code;

  constructor(
    private userService: UserService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.getAllDealRegWithCommentsInfo();
  }

  editCommentId;
  getCommentId(dealID, dealCmd) {
    this.editedComments = dealCmd;
    this.editCommentId = dealID;
  }

  private apiResponseData: any = null;
  getAllDealRegWithCommentsInfo() {
    this.loading = true;
    this.apiResponseData = null;
    this.userService.GetDealRegInfoWithComments_ADO()
      .subscribe((response) => {
        console.log(response);
        this.apiResponseData = response;
      },
        error => {
          this.alertService.error(error);
          this.error=error;
          this.loading = false;
        }, () => {
          this.loading = false;
          if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
            var resultString = this.apiResponseData[0].data;
            var resultArray = JSON.parse(resultString);
            if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
              this.geos = resultArray['Table'].reverse();
              console.log(this.geos);
            }
          } else {
            this.alertService.error("No Response");
            this.loading = false;
          }
        })
    
  }

  updateComment() {

    let statusDetail = {
      DealId: this.editCommentId,
      DealComment: this.editedComments
    }
    if ((this.editCommentId).toString().trim() != "" && (this.editedComments).toString().trim() != "") {
      this.loading = true;
      this.apiResponseData = null;
      this.userService.updateCommentService(statusDetail)
        .subscribe((response) => {
          console.log(response);
          this.apiResponseData = response;
        },
          error => {
            this.alertService.error(error);
            this.loading = false;
          }, () => {
            this.loading = false;
            if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
              var resultString = this.apiResponseData[0].data;
              var resultArray = JSON.parse(resultString);
              if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
                this.alertService.success("Updated Successfully !");
                this.geos = resultArray['Table'].reverse();
              }
            } else {
              this.alertService.error("No Response");
              this.loading = false;
            }
          });
    } else {
      this.alertService.error("Please select deal and enter comments");
      this.loading = false;
    }
  }

  viewComments(DealRegID){
    this.userService.viewCommentsService(DealRegID)
    .subscribe((response) => {
      this.apiResponseData = response;
    },
      error => {
        this.alertService.error(error);
        this.error=error;
        this.loading = false;
      }, () => {
        this.loading = false;
        if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
          var resultString = this.apiResponseData[0].data;
          var resultArray = JSON.parse(resultString);
          if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
          this.commentDetails = resultArray['Table'];

            if(this.commentDetails[0].Code === 20){
              this.code = 20;
              this.commentDetails = resultArray['Table'];
            }
            else {
              this.code = 2;
              this.commentDetails = resultArray['Table'];
            }
          }
          else {
            this.code = 2;
            this.commentDetails = resultArray['Table'];
  
          }
        } else {
          this.alertService.error("No Response");
          this.loading = false;
        }
      })
  
}
 
}
