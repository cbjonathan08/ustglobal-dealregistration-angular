import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/project';

@Component({
  selector: 'app-rolepermissions',
  templateUrl: './rolepermissions.component.html',
  styleUrls: ['./rolepermissions.component.css']
})
export class RolepermissionsComponent implements OnInit {

  loading = false;
  projects: Project[] = [];
  newProject: Project = new Project();
  editProject: Project = new Project();
  editIndex: number = null;
  deleteProject: Project = new Project();
  deleteIndex: number = null;
  searchBy: string = "ProjectName";
  searchText: string = "";

   RolePermissions = [ 
    {RoleName:'Admin', CanAdd: 'Yes', CanView: 'Yes', CanEdit: 'Yes', CanDelete: 'Yes', CanNotify: 'Yes', CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020'},
    {RoleName:'Channel Partner', CanAdd: 'Yes', CanView: 'Yes', CanEdit: 'Yes', CanDelete: 'No', CanNotify: 'Yes', CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020'},
    {RoleName:'UST Employee', CanAdd: 'Yes', CanView: 'Yes', CanEdit: 'Yes', CanDelete: 'No', CanNotify: 'Yes', CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020'},
    {RoleName:'Cloud Pick Employee', CanAdd: 'Yes', CanView: 'Yes', CanEdit: 'Yes', CanDelete: 'No', CanNotify: 'Yes', CreatedBy: 'Patan Dadapeer Khan', CreatedOn: '19/01/2020',  ModifiedBy: 'Nitin Dutt', ModifiedOn: '19/01/2020'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
