import { Component, OnInit } from '@angular/core';
import { DealStatusService, AlertService, AuthenticationService } from "./../../_services";
import { DealStatus } from './../../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dealstatus',
  templateUrl: './dealstatus.component.html',
  styleUrls: ['./dealstatus.component.css']
})
export class DealstatusComponent implements OnInit {

  dealStatusList: DealStatus[] = [];

  // New Entry
  dealStatusForm: FormGroup;
  loading = false;
  submitted = false;
  display = 'none';
  createdBy;
  modifiedBy;

  // Edit Entry
  editDealStatusForm: FormGroup;
  editLoading = false;
  editSubmitted = false;
  editDisplay = 'none';
  editIndex: number = null;

  // Delete Entry
  deleteDealStatusForm: FormGroup;
  deleteLoading = false;
  deleteSubmitted = false;
  deleteDisplay = 'none';
  deleteIndex: number = null;

  idOfCurrentUser;
  error;

  constructor(
    private dealStatusService: DealStatusService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
  }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
  }

  ngOnInit() {

    this.getValueFromLocalStorage();

    this.dealStatusForm = this.formBuilder.group({
      dealStatusName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.editDealStatusForm = this.formBuilder.group({
      dealStatusID: [null, [Validators.required]],
      dealStatusName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.deleteDealStatusForm = this.formBuilder.group({
      dealStatusID: [null, [Validators.required]],
      dealStatusName: [null, [Validators.required]]
    });


    // Grid binding
   this.getAllDealStatus();
  }

  // convenience getter for easy access to form fields
  get f() { return this.dealStatusForm.controls; }
  get fedit() { return this.editDealStatusForm.controls; }
  get fdelete() { return this.deleteDealStatusForm.controls; }

  openModal() {
    this.display = 'block';
  }

  onCloseHandled() {
    this.submitted = false;
    this.loading = false;
    this.dealStatusForm.reset();
    this.display = 'none';
  }

  onEditCloseHandled() {
    this.editSubmitted = false;
    this.editLoading = false;
    this.editDealStatusForm.reset();
    this.editDisplay = 'none';
  }

  onDeleteCloseHandled() {
    this.deleteSubmitted = false;
    this.deleteLoading = false;
    this.deleteDealStatusForm.reset();
    this.deleteDisplay = 'none';
  }

  getAllDealStatus(){
    let dealStatusDetails= {
      UserId:this.idOfCurrentUser,
      Type:'status'
    }
    this.dealStatusService.getAllDealStatusService(dealStatusDetails).subscribe(
      (response) => {
        var resultString = JSON.parse(response[0].data);
        this.dealStatusList = resultString['Table'].reverse();
        console.log(this.dealStatusList);
       
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
        // this.error = error;
        // this.loading = false;
      }
    );
  }

  onSaveClick() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.dealStatusForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.dealStatusForm.value, null, 4));

    this.loading = true;
    this.dealStatusForm.value.createdBy = this.idOfCurrentUser;
    this.dealStatusService.insert(this.dealStatusForm.value)
      .pipe(first())
      .subscribe(
        (response) => {
     
        
          //Add Geo to Grid
          var p: DealStatus = new DealStatus();
          p.dealStatusID = response.dealStatusID;
          p.dealStatusName = response.dealStatusName;
          p.createdBy = response.createdBy;
          p.createdOn = response.createdOn;
          p.modifiedBy = response.modifiedBy;
          p.modifiedOn = response.modifiedOn;
          this.dealStatusList.push(p);

          this.alertService.success('Deal Status created successfully!', true);
          // this.error ="Registration successful";
          this.onCloseHandled();
          this.getAllDealStatus();


        }, (error) => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onEditClick(event, index: number) {

    this.editDisplay = 'block';
    this.editIndex = index;

    this.editDealStatusForm.setValue({
      dealStatusID: this.dealStatusList[index].dealStatusID,
      dealStatusName: this.dealStatusList[index].dealStatusName,
      createdBy:event.UserID,
      modifiedBy:this.idOfCurrentUser
    });

  }

  onUpdateClick() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editDealStatusForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.editDealStatusForm.value, null, 4));

    this.editLoading = true;

    this.dealStatusService.update(this.editDealStatusForm.value)
      .pipe(first())
      .subscribe((response: DealStatus) => {

        var p: DealStatus = new DealStatus();
        p.dealStatusID = response.dealStatusID;
        p.dealStatusName = response.dealStatusName;
        p.createdBy = response.createdBy;
        p.createdOn = response.createdOn;
        p.modifiedBy = response.modifiedBy;
        p.modifiedOn = response.modifiedOn;
        this.dealStatusList[this.editIndex] = p;

        this.alertService.success('Deal Status updated successfully!', true);
        // this.error ="Registration successful";
        this.onEditCloseHandled();
        this.getAllDealStatus();

      },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });

  }

  onDeleteClick(event, index: number) {

    this.deleteDisplay = 'block';
    this.deleteIndex = index;

    this.deleteDealStatusForm.setValue({
      dealStatusID: this.dealStatusList[index].dealStatusID,
      dealStatusName: this.dealStatusList[index].dealStatusName
    });
  }

  onDeleteConfirmClick() {

    this.deleteSubmitted = true;

    // stop here if form is invalid
    if (this.deleteDealStatusForm.invalid) {
      return;
    }

    // display form values on success
   //salert('SUCCESS!! :-)\n\n' + JSON.stringify(this.deleteDealStatusForm.value, null, 4));
  
    this.deleteLoading = true;

    this.dealStatusService.delete(this.deleteDealStatusForm.value.dealStatusID)
      .pipe(first())
      .subscribe(
        (response) => {
         
          this.dealStatusList.splice(this.deleteIndex, 1);

          this.alertService.success('Deal Status deleted successfully!', true);
          // this.error ="Registration successful";
          this.onDeleteCloseHandled();

        },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });
  }

}
