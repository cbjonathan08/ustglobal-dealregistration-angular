import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthenticationService, UserService, AlertService } from 'src/app/_services';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  reportType = 0;
  statusType = 0;
  fromDate ='';
  toDate ='';
  reportDetails;
  noRecordFound;
  resultStatus;
  error;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService
  ) { }

  typeOfReport = [
    {
      "reportId":1,
      "reportName":'Deal Report'
    },
    {
      "reportId":2,
      "reportName":'Member Report'
    }
  ]

  dealStatusList;
  getAllDealStatus(){
      // Grid binding
      this.userService.getAllDealStatusService().subscribe(
        (response) => {
          this.dealStatusList = response;
          console.log(this.dealStatusList);
        },
        (error) => {
          console.log(error);
          this.alertService.error(error);
          this.error=error;
          // this.error = error;
        }
      );
  }

  changeReportType(){
    this.noRecordFound = false;
    this.reportDetails = [];
    //console.log(this.reportType);
    if(this.reportType === 2){
      this.statusType = 0;
    }

  }

  changeStatusType(){
    this.noRecordFound = false;
    this.reportDetails = [];
    //console.log(this.statusType);
  }

  selectFromDate(){
    this.noRecordFound = false;
    this.reportDetails = [];
    //console.log(this.fromDate);
  }

  selectToDate(){
    this.noRecordFound = false;
    this.reportDetails = [];
    //console.log(this.toDate);
  }

  generateReport(){
    if(this.reportType === 0 ||  this.fromDate === '' || this.toDate === '' ){
      this.alertService.error("Please select all required fields");
    }
    console.log(this.reportType);
    let generateReportDetails = {
      Type:this.reportType,
      DealStatusID: this.statusType,
      StartDate: this.fromDate,
      LastDate: this.toDate
     
    }
  

    this.userService.generateReportService(generateReportDetails).subscribe(
      (response) => {
        console.log(response);
        var resultString = JSON.parse(response[0].data);
        this.resultStatus = response[0].status;
        if(this.resultStatus && resultString !== 0){
          this.reportDetails = resultString['Table'].reverse();
          if(this.reportDetails === undefined){
            this.noRecordFound = true;
          }
          console.log( this.reportDetails);
        }
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
       
      }
    );
  
  }

  ngOnInit() {
    this.getAllDealStatus();
    var date = new Date();

    // add a day
    date.setDate(date.getDate() + 1);
    console.log(date);
  }

}
