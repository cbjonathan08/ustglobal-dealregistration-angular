import { Component, OnInit } from '@angular/core';
import { RolesService, AlertService, AuthenticationService } from "./../../_services";
import { Roles } from './../../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']

})
export class RoleComponent implements OnInit {

  roles: Roles[] = [];
  error;
  

  // New Entry
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  display = 'none';
  // error = '';

  // Edit Entry
  editLoginForm: FormGroup;
  editLoading = false;
  editSubmitted = false;
  editDisplay = 'none';
  editIndex: number = null;

  // Delete Entry
  deleteLoginForm: FormGroup;
  deleteLoading = false;
  deleteSubmitted = false;
  deleteDisplay = 'none';
  deleteIndex: number = null;

  idOfCurrentUser;
  respondFromServer;
  idToDelete;
  createdBy;
  modifiedBy;

  constructor(
    private rolesService: RolesService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
  }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    console.log("id from localstorage",this.idOfCurrentUser);
  }

  id;


  ngOnInit() {
    this.getValueFromLocalStorage();
  
    this.loginForm = this.formBuilder.group({
      roleName: [null, [Validators.required]],
      roleDesc: [null]
    });

    this.editLoginForm = this.formBuilder.group({
      roleID: [null, [Validators.required]],
      roleName: [null, [Validators.required]],
      roleDesc: [null],
      createdBy:[null],
      modifiedBy:[null]
    
    });

    this.deleteLoginForm = this.formBuilder.group({
      roleID: [null, [Validators.required]],
      roleName: [null, [Validators.required]],
      roleDesc: [null]
    });

   
    let roleDetails= {
      UserId:this.idOfCurrentUser,
      Type:'Role'
    }

    this.getAllRoles();
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }
  get fedit() { return this.editLoginForm.controls; }
  get fdelete() { return this.deleteLoginForm.controls; }

  openModal() {
    this.display = 'block';
  }

  onCloseHandled() {
    this.submitted = false;
    this.loading = false;
    this.loginForm.reset();
    this.display = 'none';
  }

  onEditCloseHandled() {
    this.editSubmitted = false;
    this.editLoading = false;
    this.editLoginForm.reset();
    this.editDisplay = 'none';
  }

  onDeleteCloseHandled() {
    this.deleteSubmitted = false;
    this.deleteLoading = false;
    this.deleteLoginForm.reset();
    this.deleteDisplay = 'none';
  }

  

  getAllRoles(){
    let roleDetails= {
      UserId:this.idOfCurrentUser,
      Type:'Role'
    }
    this.rolesService.getAllRolesService(roleDetails).subscribe(
      (response) => {
        var resultString = JSON.parse(response[0].data);
        this.roles = resultString['Table'].reverse();
        console.log(this.roles);
       
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
       
        // this.loading = false;
      }
    );
  }


  onSaveClick() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value, null, 4));

    this.loading = true;
    this.loginForm.value.createdBy = this.idOfCurrentUser;
    this.rolesService.insertProject(this.loginForm.value)
   
      .pipe(first())
      .subscribe(
        (response) => {
          //console.log(response);
          //Add Project to Grid
          var p: Roles = new Roles();
          p.roleID = response.roleID;
          p.roleName = response.roleName;
          p.roleDesc = response.roleDesc;
          p.createdBy = response.createdBy;
          p.createdOn = response.createdOn;
          p.modifiedBy = response.modifiedBy;
          p.modifiedOn = response.modifiedOn;
          this.roles.push(p);

          //Clear New Project Dialog - TextBoxes
          // this.newRole.roleID = null;
          // this.newRole.roleName = null;
          // this.newRole.roleDesc = null;
          // this.newRole.createdBy = null;
          // this.newRole.createdOn = null;
          // this.newRole.modifiedBy = null;
          // this.newRole.modifiedOn = null;
          this.getAllRoles();
          this.alertService.success('Role created successfully!', true);
          // this.error ="Registration successful";
          this.onCloseHandled();


        }, (error) => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onEditClick(event, index: number) {
    console.log(event);
    // this.editDisplay = 'block';
    // this.editRole.roleID = this.roles[index].roleID;
    // this.editRole.roleName = this.roles[index].roleName;
    // this.editRole.roleDesc = this.roles[index].roleDesc;
    // this.editIndex = index;

    // this.editDisplay = 'block';
    // this.editIndex = index;

    // this.editLoginForm.setValue({
    //   roleID: this.roles[index].roleID,
    //   roleName: this.roles[index].roleName,
    //   roleDesc: this.roles[index].roleDesc
    // });
    this.editDisplay = 'block';
    this.editIndex = index;

    this.editLoginForm.setValue({
      roleID: event.RoleID,
      roleName: event.RoleName,
      roleDesc: event.RoleDesc,
      createdBy:event.UserID,
      modifiedBy:this.idOfCurrentUser
    
    });

  }

  onUpdateClick() {
    this.editSubmitted = true;
    console.log(this.editLoginForm.value);

    // stop here if form is invalid
    if (this.editLoginForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.editLoginForm.value, null, 4));
  
    this.editLoading = true;

    this.rolesService.updateProject(this.editLoginForm.value)
      .pipe(first())
      .subscribe((response: Roles) => {

        var p: Roles = new Roles();
        p.roleID = response.roleID;
        p.roleName = response.roleName;
        p.roleDesc = response.roleDesc;
        p.createdBy = response.createdBy;
        p.createdOn = response.createdOn;
        p.modifiedBy = response.modifiedBy;
        p.modifiedOn = response.modifiedOn;
        this.roles[this.editIndex] = p;

        this.alertService.success('Role updated successfully!', true);
        // this.error ="Registration successful";
        this.onEditCloseHandled();
        this.getAllRoles();

      },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });

  }


  onDeleteClick(roleDetails) {
  this.idToDelete = roleDetails.RoleID;
  console.log(roleDetails.RoleID);
      this.deleteDisplay = 'block';
      //this.deleteIndex = index;
  
      // this.deleteLoginForm.setValue({
      //   roleID: this.roles[index].roleID,
      //   roleName: this.roles[index].roleName,
      //   roleDesc: this.roles[index].roleDesc
      // });
  }


  onDeleteConfirmClick() {

    this.deleteSubmitted = true;

    // stop here if form is invalid
    // if (this.deleteLoginForm.invalid) {
    //   return;
    // }


    this.deleteLoading = true;

    this.rolesService.deleteProject(this.idToDelete)
      .pipe(first())
      .subscribe(
        (response) => {
          this.roles.splice(this.deleteIndex, 1);
          this.alertService.success('Role deleted successfully!', true);
          // this.error ="Registration successful";
          this.onDeleteCloseHandled();
          this.getAllRoles();

        },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });
  }
}
