import { Component, OnInit } from '@angular/core';
import { StateService, CountryService, AlertService, AuthenticationService } from "./../../_services";
import { State, Country } from './../../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {

  states: State[] = [];
  countries: Country[] = [];

  // New Entry
  stateForm: FormGroup;
  loading = false;
  submitted = false;
  display = 'none';
  error;
  createdBy;
  modifiedBy;


  // Edit Entry
  editStateForm: FormGroup;
  editLoading = false;
  editSubmitted = false;
  editDisplay = 'none';
  editIndex: number = null;

  // Delete Entry
  deleteStateForm: FormGroup;
  deleteLoading = false;
  deleteSubmitted = false;
  deleteDisplay = 'none';
  deleteIndex: number = null;

  idOfCurrentUser;

  constructor(
    private stateService: StateService,
    private countryService: CountryService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
  }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    console.log("id from localstorage",this.idOfCurrentUser);
  }

  ngOnInit() {

    this.getValueFromLocalStorage();

    this.stateForm = this.formBuilder.group({
      stateName: [null, [Validators.required]],
      countryID: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.editStateForm = this.formBuilder.group({
      stateID: [null, [Validators.required]],
      countryID: [null, [Validators.required]],
      stateName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.deleteStateForm = this.formBuilder.group({
      stateID: [null, [Validators.required]],
      countryID: [null, [Validators.required]],
      stateName: [null, [Validators.required]]
    });

    let stateDetails= {
      UserId:this.idOfCurrentUser,
      Type:'State'
    }

    this.countryService.getAllCountries().subscribe(
      (response: Country[]) => {
        this.countries = response;
      },
      (error) => {
        this.alertService.error(error);
      }
    );
    
    // Grid binding
    this.getAllState();
  }

  // convenience getter for easy access to form fields
  get f() { return this.stateForm.controls; }
  get fedit() { return this.editStateForm.controls; }
  get fdelete() { return this.deleteStateForm.controls; }

  openModal() {
    this.display = 'block';
  }

  onCloseHandled() {
    this.submitted = false;
    this.loading = false;
    this.stateForm.reset();
    this.display = 'none';
  }

  onEditCloseHandled() {
    this.editSubmitted = false;
    this.editLoading = false;
    this.editStateForm.reset();
    this.editDisplay = 'none';
  }

  onDeleteCloseHandled() {
    this.deleteSubmitted = false;
    this.deleteLoading = false;
    this.deleteStateForm.reset();
    this.deleteDisplay = 'none';
  }

 

  getAllState(){

    let stateDetails= {
      UserId:this.idOfCurrentUser,
      Type:'State'
    }

    this.stateService.getAllStateService(stateDetails).subscribe(
      (response) => {
        var resultString = JSON.parse(response[0].data);
        this.states = resultString['Table'].reverse();
        console.log(this.states);
       
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
        // this.error = error;
        // this.loading = false;
      }
    );
  }



  onSaveClick() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.stateForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.stateForm.value, null, 4));

    this.loading = true;
    this.stateForm.value.createdBy = this.idOfCurrentUser;
    this.stateService.insert(this.stateForm.value)
      .pipe(first())
      .subscribe(
        (response) => {
          //Add Geo to Grid
          var p: State = new State();
          p.stateID = response.stateID;
          p.stateName = response.stateName;
          p.countryID = response.countryID;
          p.countryName = response.countryName;
          p.createdBy = response.createdBy;
          p.createdOn = response.createdOn;
          p.modifiedBy = response.modifiedBy;
          p.modifiedOn = response.modifiedOn;
          this.states.push(p);

          this.alertService.success('State created successfully!', true);
          // this.error ="Registration successful";
          this.onCloseHandled();
          this.getAllState();

        }, (error) => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onEditClick(event, index: number) {
    console.log(event);

    this.editDisplay = 'block';
    this.editIndex = index;

    this.editStateForm.setValue({
      countryID: this.states[index].countryID,
      stateID: this.states[index].stateID,
      stateName: this.states[index].stateName,
      createdBy:event.UserID,
      modifiedBy:this.idOfCurrentUser
    });

  }

  onUpdateClick() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editStateForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.editStateForm.value, null, 4));
   
    this.editLoading = true;

    this.stateService.update(this.editStateForm.value)
      .pipe(first())
      .subscribe((response: State) => {

        this.alertService.success('State updated successfully!', true);
        // this.error ="Registration successful";
        this.onEditCloseHandled();
        this.getAllState();

      },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });

  }

  onDeleteClick(event, index: number) {

    this.deleteDisplay = 'block';
    this.deleteIndex = index;

    this.deleteStateForm.setValue({
      countryID: this.states[index].countryID,
      stateID: this.states[index].stateID,
      stateName: this.states[index].stateName
    });
  }

  onDeleteConfirmClick() {

    this.deleteSubmitted = true;

    // stop here if form is invalid
    if (this.deleteStateForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.deleteStateForm.value, null, 4));
   
    this.deleteLoading = true;

    this.stateService.delete(this.deleteStateForm.value.stateID)
      .pipe(first())
      .subscribe(
        (response) => {
         
          this.states.splice(this.deleteIndex, 1);

          this.alertService.success('State deleted successfully!', true);
          // this.error ="Registration successful";
          this.onDeleteCloseHandled();

        },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });
  }

}
