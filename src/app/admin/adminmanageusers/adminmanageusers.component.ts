import { Component, OnInit } from '@angular/core';
import { AlertService, UserService, AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-adminmanageusers',
  templateUrl: './adminmanageusers.component.html',
  styleUrls: ['./adminmanageusers.component.css']
})
export class AdminmanageusersComponent implements OnInit {

  loading = false;
  edited = {};
  userId;
  user;
  error;;


  constructor(
    private userService : UserService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers(){
    this.userService.getAllUsersService().subscribe((response) =>
    {
     var resultString = JSON.parse(response[0].data);
     this.user = resultString['Table'].reverse();
     console.log(this.user);

    },
    error => {
     console.log(error);
     this.alertService.error(error); 
     this.error=error;
   }
    );
  }

  editUser(details){
    this.edited=details;
    console.log(this.edited);
  }

  adminUpdateUser(){
    this.userService.adminUpdateUserService(this.edited).subscribe((response) =>
    {
     console.log(response);
     var resultStatus = JSON.parse(response[0].status);
     if(resultStatus === true){
      this.alertService.success('Successfully updated');
     }
     else {
      this.alertService.error('Update fail'); 
     }
    },
    error => {
     console.log(error);
     this.alertService.error(error); 
  }
    );

  }

  confirmDeleteUser(userId){
    this.userId = userId;
  }

  deleteUser(){
    this.userService.deleteUserService(this.userId).subscribe((response) =>
    {
     console.log(response);
     var resultStatus = JSON.parse(response[0].status);
     if(resultStatus === true){
      this.alertService.success('Successfully deleted');
      this.getAllUsers();
     }
     else {
      this.alertService.error('Delete fail'); 
     }
    },
    error => {
     console.log(error);
     this.alertService.error(error); 
  }
    );
  }


}

