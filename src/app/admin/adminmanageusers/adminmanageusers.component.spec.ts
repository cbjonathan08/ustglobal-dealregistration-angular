import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminmanageusersComponent } from './adminmanageusers.component';

describe('AdminmanageusersComponent', () => {
  let component: AdminmanageusersComponent;
  let fixture: ComponentFixture<AdminmanageusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminmanageusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminmanageusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
