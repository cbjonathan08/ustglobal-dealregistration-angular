import { Component, OnInit } from '@angular/core';
import { ProjectService, AlertService, AuthenticationService } from "./../../_services";
import { Project } from './../../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  projects: Project[] = [];

  // New Entry
  projectForm: FormGroup;
  loading = false;
  submitted = false;
  display = 'none';
  createdBy;
  modifiedBy;

  // Edit Entry
  editProjectForm: FormGroup;
  editLoading = false;
  editSubmitted = false;
  editDisplay = 'none';
  editIndex: number = null;

  // Delete Entry
  deleteProjectForm: FormGroup;
  deleteLoading = false;
  deleteSubmitted = false;
  deleteDisplay = 'none';
  deleteIndex: number = null;

  idOfCurrentUser;
  error;

  constructor(
    private projectService: ProjectService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
  }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
  }


  ngOnInit() {

    this.getValueFromLocalStorage();

    this.projectForm = this.formBuilder.group({
      projectName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.editProjectForm = this.formBuilder.group({
      projectID: [null, [Validators.required]],
      projectName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.deleteProjectForm = this.formBuilder.group({
      projectID: [null, [Validators.required]],
      projectName: [null, [Validators.required]]
    });

  

    // Grid binding
    this.getAllProjects();
  }

  // convenience getter for easy access to form fields
  get f() { return this.projectForm.controls; }
  get fedit() { return this.editProjectForm.controls; }
  get fdelete() { return this.deleteProjectForm.controls; }

  openModal() {
    this.display = 'block';
  }

  onCloseHandled() {
    this.submitted = false;
    this.loading = false;
    this.projectForm.reset();
    this.display = 'none';
  }

  onEditCloseHandled() {
    this.editSubmitted = false;
    this.editLoading = false;
    this.editProjectForm.reset();
    this.editDisplay = 'none';
  }

  onDeleteCloseHandled() {
    this.deleteSubmitted = false;
    this.deleteLoading = false;
    this.deleteProjectForm.reset();
    this.deleteDisplay = 'none';
  }


  getAllProjects(){
    let projectDetails= {
      UserId:this.idOfCurrentUser,
      Type:'Project'
    }
    this.projectService.getAllProjectService(projectDetails).subscribe(
      (response) => {
        var resultString = JSON.parse(response[0].data);
        this.projects = resultString['Table'].reverse();
        console.log(this.projects);
       
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
        // this.error = error;
        // this.loading = false;
      }

    );
  }
  onSaveClick() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.projectForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.projectForm.value, null, 4));

    this.loading = true;
    this.projectForm.value.createdBy = this.idOfCurrentUser;
    this.projectService.insert(this.projectForm.value)
      .pipe(first())
      .subscribe(
        (response) => {
          debugger;
          //Add Geo to Grid
          var p: Project = new Project();
          p.projectID = response.projectID;
          p.projectName = response.projectName;
          p.createdBy = response.createdBy;
          p.createdOn = response.createdOn;
          p.modifiedBy = response.modifiedBy;
          p.modifiedOn = response.modifiedOn;
          this.projects.push(p);

          this.alertService.success('Project created successfully!', true);
          // this.error ="Registration successful";
          this.onCloseHandled();
          this.getAllProjects();

        }, (error) => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onEditClick(event, index: number) {

    this.editDisplay = 'block';
    this.editIndex = index;

    this.editProjectForm.setValue({
      projectID: this.projects[index].projectID,
      projectName: this.projects[index].projectName,
      createdBy:event.UserID,
      modifiedBy:this.idOfCurrentUser
    });

  }

  onUpdateClick() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editProjectForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.editProjectForm.value, null, 4));
  
    this.editLoading = true;

    this.projectService.update(this.editProjectForm.value)
      .pipe(first())
      .subscribe((response: Project) => {

        var p: Project = new Project();
        p.projectID = response.projectID;
        p.projectName = response.projectName;
        p.createdBy = response.createdBy;
        p.createdOn = response.createdOn;
        p.modifiedBy = response.modifiedBy;
        p.modifiedOn = response.modifiedOn;
        this.projects[this.editIndex] = p;

        this.alertService.success('Project updated successfully!', true);
        // this.error ="Registration successful";
        this.onEditCloseHandled();
        this.getAllProjects();

      },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });

  }

  onDeleteClick(event, index: number) {

    this.deleteDisplay = 'block';
    this.deleteIndex = index;

    this.deleteProjectForm.setValue({
      projectID: this.projects[index].projectID,
      projectName: this.projects[index].projectName
    });
  }

  onDeleteConfirmClick() {

    this.deleteSubmitted = true;

    // stop here if form is invalid
    if (this.deleteProjectForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.deleteProjectForm.value, null, 4));
    debugger;
    this.deleteLoading = true;

    this.projectService.delete(this.deleteProjectForm.value.projectID)
      .pipe(first())
      .subscribe(
        (response) => {
          debugger;
          this.projects.splice(this.deleteIndex, 1);

          this.alertService.success('Project deleted successfully!', true);
          // this.error ="Registration successful";
          this.onDeleteCloseHandled();

        },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });
  }

}
