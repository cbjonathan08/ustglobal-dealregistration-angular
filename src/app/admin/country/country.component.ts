import { Component, OnInit } from '@angular/core';
import { CountryService, AlertService, AuthenticationService } from "./../../_services";
import { Country } from './../../_models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {

  countries: Country[] = [];

  // New Entry
  countryForm: FormGroup;
  loading = false;
  submitted = false;
  display = 'none';

  // Edit Entry
  editCountryForm: FormGroup;
  editLoading = false;
  editSubmitted = false;
  editDisplay = 'none';
  editIndex: number = null;

  // Delete Entry
  deleteCountryForm: FormGroup;
  deleteLoading = false;
  deleteSubmitted = false;
  deleteDisplay = 'none';
  deleteIndex: number = null;

  idOfCurrentUser;
  firstName;
  lastName;
  error;
  createdBy;
  modifiedBy;

  constructor(
    private countryService: CountryService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
  }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    this.firstName = currentUser.firstName;
    this.lastName = currentUser.lastName;
    console.log("id from localstorage",this.idOfCurrentUser);
  }

  ngOnInit() {

    this.getValueFromLocalStorage();

    this.countryForm = this.formBuilder.group({
      countryName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.editCountryForm = this.formBuilder.group({
      countryID: [null, [Validators.required]],
      countryName: [null, [Validators.required]],
      createdBy:[null],
      modifiedBy:[null]
    });

    this.deleteCountryForm = this.formBuilder.group({
      countryID: [null, [Validators.required]],
      countryName: [null, [Validators.required]]
    });

    let countryDetails= {
      UserId:this.idOfCurrentUser,
      Type:'Country'
    }

    // Grid binding
    this.getAllCountry();
  }

  // convenience getter for easy access to form fields
  get f() { return this.countryForm.controls; }
  get fedit() { return this.editCountryForm.controls; }
  get fdelete() { return this.deleteCountryForm.controls; }

  openModal() {
    this.display = 'block';
  }

  onCloseHandled() {
    this.submitted = false;
    this.loading = false;
    this.countryForm.reset();
    this.display = 'none';
  }

  onEditCloseHandled() {
    this.editSubmitted = false;
    this.editLoading = false;
    this.editCountryForm.reset();
    this.editDisplay = 'none';
  }

  onDeleteCloseHandled() {
    this.deleteSubmitted = false;
    this.deleteLoading = false;
    this.deleteCountryForm.reset();
    this.deleteDisplay = 'none';
  }


  onSaveClick() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.countryForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.countryForm.value, null, 4));

    this.loading = true;
    this.countryForm.value.createdBy = this.idOfCurrentUser;
    this.countryService.insertCountry(this.countryForm.value)
      .pipe(first())
      .subscribe(
        (response) => {
      
          //Add Geo to Grid
          var p: Country = new Country();
          p.countryID = response.countryID;
          p.countryName = response.countryName;
          p.createdBy = response.createdBy;
          p.createdOn = response.createdOn;
          p.modifiedBy = response.modifiedBy;
          p.modifiedOn = response.modifiedOn;
          this.countries.push(p);

          this.alertService.success('Country created successfully!', true);
          // this.error ="Registration successful";
          this.onCloseHandled();
          this.getAllCountry();


        }, (error) => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onEditClick(event, index: number) {

    this.editDisplay = 'block';
    this.editIndex = index;

    this.editCountryForm.setValue({
      countryID: this.countries[index].countryID,
      countryName: this.countries[index].countryName,
      createdBy:event.UserID,
      modifiedBy:this.idOfCurrentUser
    });

  }

  
  getAllCountry(){

    let countryDetails= {
      UserId:this.idOfCurrentUser,
      Type:'Country'
    }

    this.countryService.getAllCountriesService(countryDetails).subscribe(
      (response) => {
        var resultString = JSON.parse(response[0].data);
        this.countries = resultString['Table'].reverse();
        console.log(this.countries);
       
      },
      (error) => {
        console.log(error);
        this.alertService.error(error);
        // this.error = error;
        // this.loading = false;
      }
    );
  }

  onUpdateClick() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editCountryForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.editCountryForm.value, null, 4));
    debugger;
    this.editLoading = true;

    this.countryService.updateCountry(this.editCountryForm.value)
      .pipe(first())
      .subscribe((response: Country) => {

    
        this.alertService.success('Country updated successfully!', true);
        // this.error ="Registration successful";
        this.onEditCloseHandled();
        this.getAllCountry();

      },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });

  }

  onDeleteClick(event, index: number) {

    this.deleteDisplay = 'block';
    this.deleteIndex = index;

    this.deleteCountryForm.setValue({
      countryID: this.countries[index].countryID,
      countryName: this.countries[index].countryName
    });
  }

  onDeleteConfirmClick() {

    this.deleteSubmitted = true;

    // stop here if form is invalid
    if (this.deleteCountryForm.invalid) {
      return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.deleteCountryForm.value, null, 4));
    debugger;
    this.deleteLoading = true;

    this.countryService.deleteCountry(this.deleteCountryForm.value.countryID)
      .pipe(first())
      .subscribe(
        (response) => {
          debugger;
          this.countries.splice(this.deleteIndex, 1);

          this.alertService.success('Country deleted successfully!', true);
          // this.error ="Registration successful";
          this.onDeleteCloseHandled();

        },
        (error) => {
          this.alertService.error(error);
          this.editLoading = false;
        });
  }
  
}
