import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/project';
import { Router } from '@angular/router';
import { AuthenticationService, UserService, AlertService } from 'src/app/_services';

@Component({
  selector: 'app-dealsubscribers',
  templateUrl: './dealsubscribers.component.html',
  styleUrls: ['./dealsubscribers.component.css']
})
export class DealsubscribersComponent implements OnInit {

  loading = false;

  geos = [];
  dealMappedUsers:any = [];
  onlydealMappedUsers:any = [];
  projects: Project[] = [];
  newProject: Project = new Project();
  editProject: Project = new Project();
  editIndex: number = null;
  deleteProject: Project = new Project();
  deleteIndex: number = null;
  searchBy: string = "ProjectName";
  searchText: string = "";
  CURRENT_USER:any = null;
  error;

  MemSearchBy:string = "FirstName";
  member_searchText = "";
  SELC_DEAL_ID = "";
  SELC_DEAL_DETAIS:any = {};
  display = 'none';
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.getAllDealRegInfo();
    this.CURRENT_USER = (localStorage.getItem('currentUser') != undefined) ? JSON.parse(localStorage.getItem('currentUser')) : {};
  }
  onCloseHandled() {
    this.MemSearchBy = "FirstName";
    this.member_searchText = "";
    this.SELC_DEAL_ID = "";
    this.SELC_DEAL_DETAIS = {};
    this.display = 'none';
  }
  private apiResponseData: any = null;
  getAllDealRegInfo() {
    this.loading = true;
    this.apiResponseData = null;
    
    this.userService.GetDealRegInfo_ADO()
      .subscribe((response) => {
        console.log(response);
        this.apiResponseData = response;
      },
        error => {
          this.alertService.error(error);
          this.error=error;
          this.loading = false;
        }, () => {
          this.loading = false;
          if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
            var resultString = this.apiResponseData[0].data;
            var resultArray = JSON.parse(resultString);
            if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
              this.geos = resultArray['Table'].reverse();
              console.log(this.geos);
            }
          } else {
            this.alertService.error("No Response");
            this.loading = false;
          }
        })
  }
  getAllDealMappedMemberInfo(dealID:any, dealDetail:any) {
    this.dealMappedUsers = [];
    let statusDetail = {
      DealRegID: dealID,
    }
    if (dealID.toString().trim() != "") {
      this.SELC_DEAL_ID = dealID;
      this.SELC_DEAL_DETAIS = dealDetail;
      this.loading = true;
      this.apiResponseData = null;
      this.userService.GetDealMappedSubscribers_ADO(statusDetail)
        .subscribe((response) => {
          console.log(response);
          this.apiResponseData = response;
        },
          error => {
            this.alertService.error(error);
            this.loading = false;
          }, () => {
            this.loading = false;
            if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
              var resultString = this.apiResponseData[0].data;
              var resultArray = JSON.parse(resultString);
              if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
                this.dealMappedUsers = resultArray['Table'];
                this.onlydealMappedUsers = this.dealMappedUsers.filter(item => item.dealMapStatus == 1);
                console.log(this.dealMappedUsers);
              }
            } else {
              this.alertService.error("No Response");
              this.loading = false;
            }
          });
    }else {
      this.alertService.error("Please select deals");
      this.loading = false;
    }
  }
  addOrRemoveSubscribe(memberDetail:any, type:string,emailID:String) {
    console.log(emailID)
    this.dealMappedUsers = [];
    if (this.SELC_DEAL_ID.toString().trim() != "" && memberDetail.UserID.toString().trim() != "") {
      let statusDetail = {
        DealRegID : this.SELC_DEAL_ID,
        UserID : memberDetail.UserID,
        Status : (type == "add") ? 1 : 0,
        CreatedBy: (this.CURRENT_USER.id != undefined) ? this.CURRENT_USER.id : 0,
        emailID:emailID
      }
      this.loading = true;
      this.apiResponseData = null;
      this.userService.AddOrRemoveSubscribers_ADO(statusDetail)
        .subscribe((response) => {
          console.log(response);
          this.apiResponseData = response;
        },
          error => {
            this.alertService.error(error);
            this.loading = false;
          }, () => {
            this.loading = false;
            if (this.apiResponseData != null && this.apiResponseData.length > 0 && this.apiResponseData[0].status === true) {
              var resultString = this.apiResponseData[0].data;
              var resultArray = JSON.parse(resultString);
              if (resultArray['Table'] != undefined && resultArray['Table'].length > 0) {
                this.alertService.success("Updated Successfully !");
                this.dealMappedUsers = resultArray['Table'];
                this.onlydealMappedUsers = this.dealMappedUsers.filter(item => item.dealMapStatus == 1);
                console.log(this.dealMappedUsers);
              }
            } else {
              this.alertService.error("No Response");
              this.loading = false;
            }
          });
    }else {
      this.alertService.error("Please select deals");
      this.loading = false;
    }
  }
}
