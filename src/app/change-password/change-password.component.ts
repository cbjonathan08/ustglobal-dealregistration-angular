import { Component, OnInit } from '@angular/core';
import { AlertService, UserService, AuthenticationService } from 'src/app/_services';
import { FormBuilder, FormGroup ,Validators} from '@angular/forms';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from 'src/app/_helpers'
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  currentPassword;
  newPassword;
  confirmPassword;
  registerForm: FormGroup;
  idOfCurrentUser;
  roleName;
  changePasswordRes;
  showMessage;
  fillRequiredValue;
  showPasswordNotMatch;

  constructor(
    private userService : UserService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) { }

  getValueFromLocalStorage(){
    let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.idOfCurrentUser = currentUser.id;
    this.roleName = currentUser.role;
    console.log("id from localstorage",this.idOfCurrentUser);
  }

  ngOnInit() {
    this.getValueFromLocalStorage();
    this.registerForm = this.formBuilder.group({
      currentPassword: [null, [Validators.required, Validators.email]],
      newPassword: [null, [Validators.required, Validators.minLength(6)]],
      confirmPassword: [null, Validators.required],
    }, {
      validator: MustMatch('newPassword', 'confirmPassword')
  })
}

changePassword(){
  console.log(this.newPassword,this.confirmPassword);
  if(this.currentPassword === undefined ||  this.newPassword===undefined || this.confirmPassword === undefined){
    this.fillRequiredValue = 'Please fill in all required fields'
    this.alertService.error( this.fillRequiredValue);
  }
  else if(this.newPassword !==this.confirmPassword){
    this.showPasswordNotMatch = 'Password not match'
    this.alertService.error( this.showPasswordNotMatch);
  }
  else{
    let changePasswordDetails = {
      UserId: this.idOfCurrentUser,
      ModifiedBy: this.idOfCurrentUser,
      RoleType : this.roleName ,
      OldPassword:this.currentPassword,
      NewPassword:this.newPassword
    }
    this.userService.changePasswordService(changePasswordDetails).subscribe((response) =>
    {
      var resultString = JSON.parse(response[0].data);
      var resultStatus = JSON.parse(response[0].status);
      this.changePasswordRes = resultString['Table'];
      console.log(this.changePasswordRes);
      console.log(resultStatus);
      if(resultStatus === true){
        if(this.changePasswordRes[0].Code === 20){
            this.showMessage = this.changePasswordRes[0].descripion;
            this.alertService.error(this.showMessage);
        }
        if(this.changePasswordRes[0].Code === 10){
          this.showMessage = this.changePasswordRes[0].descripion;
          this.alertService.success(this.showMessage);
          this.currentPassword ='';
          this.newPassword ='';
          this.confirmPassword =''
      }
      }
      else{
        this.alertService.error(this.showMessage);
      }
     
    },
    error => {
     console.log(error);
     this.alertService.error(error);
     
  }
    );
  }
 

}

}
