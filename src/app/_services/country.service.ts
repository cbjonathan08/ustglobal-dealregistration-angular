import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private httpClient: HttpClient) { }

  getAllCountries(): Observable<Country[]> {
    return this.httpClient.get<Country[]>(environment.apiUrl + `/Countries`, { reportProgress: true, responseType: "json" })
      .pipe(map(
        (data: Country[]) => {
          return data;
        }
      ));
  }
  
  getAllCountriesService(roleDetails) 
  {
    console.log(roleDetails);
    return this.httpClient.post(environment.apiUrl + `/GetMasterTableRecords_ADO`, roleDetails);

  }

  insertCountry(country: Country): Observable<Country> {
    console.log(country);
    return this.httpClient.post<Country>(environment.apiUrl + `/NewCountry`, country);

  }

  updateCountry(existingProject: Country): Observable<Country> {
    console.log(existingProject);
    return this.httpClient.post<Country>(environment.apiUrl + `/UpdateCountry`, existingProject, { responseType: "json" });
  }

  deleteCountry(ID: number): Observable<string> {
    return this.httpClient.get<string>(environment.apiUrl + "/DeleteCountry?CountryID=" + ID);
  }
}
