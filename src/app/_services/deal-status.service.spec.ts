import { TestBed } from '@angular/core/testing';

import { DealStatusService } from './deal-status.service';

describe('DealStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DealStatusService = TestBed.get(DealStatusService);
    expect(service).toBeTruthy();
  });
});
