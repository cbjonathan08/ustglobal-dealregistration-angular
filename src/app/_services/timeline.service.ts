import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TimeLine } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TimelineService {

  constructor(private httpClient: HttpClient) { }

  getAllTimeLineService(timeLineDetails) 
  {
    console.log(timeLineDetails);
    return this.httpClient.post(environment.apiUrl + `/GetMasterTableRecords_ADO`, timeLineDetails);

  }

  getAllTimeLines(): Observable<TimeLine[]> {
    return this.httpClient.get<TimeLine[]>(environment.apiUrl + `/TimeLines`, { reportProgress: true, responseType: "json" })
      .pipe(map(
        (data: TimeLine[]) => {
          return data;
        }
      ));
  }

  insertTimeLine(timeLine: TimeLine): Observable<TimeLine> {
    
    return this.httpClient.post<TimeLine>(environment.apiUrl + `/NewTimeLine`, timeLine);

  }

  updateTimeLine(existingProject: TimeLine): Observable<TimeLine> {
    console.log(existingProject);
    return this.httpClient.post<TimeLine>(environment.apiUrl + `/UpdateTimeLine`, existingProject, { responseType: "json" });
  }

  deleteTimeLine(ID: number): Observable<string> {
    debugger;
    return this.httpClient.get<string>(environment.apiUrl + "/DeleteTimeLine?TimeLineID=" + ID);
  }
}
