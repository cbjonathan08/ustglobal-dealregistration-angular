import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

import { environment } from './../../environments/environment';
import { User, Roles, DealRegistration,DealRegComments } from '../_models';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserService {

  // private httpClient: HttpClient
  //apiUrl : 'http://localhost:63301/api/DealReg';

  constructor(private httpClient: HttpClient, private httpBackend: HttpBackend) { }

  getRoles(): Observable<Roles[]> {

    // this.httpClient = new HttpClient(this.httpBackend);
    return this.httpClient.get<Roles[]>(environment.apiUrl + `/roles`, { responseType: "json" });
  }

  getAll() {

    // this.httpClient = new HttpClient(this.httpBackend);
    // return this.http.get<User[]>(`${environment.apiUrl}/Employee`);
    // return this.http.get<User[]>('http://localhost:63301/api/data/authorize');
    //return this.httpClient.get<User[]>('http://localhost:63301/api/Employee');
   // return this.httpClient.get<User[]>(environment.apiUrl + '/Employee');
  }

  getById(id: number) {

    // this.httpClient = new HttpClient(this.httpBackend);
    return this.httpClient.get<User>(`${environment.apiUrl}/users/${id}`);
  }

  getUserByEmail(Email: string): Observable<any> {
    // this.httpClient = new HttpClient(this.httpBackend);
    return this.httpClient.get<any>("/api/getUserByEmail/" + Email, { responseType: "json" });
  }

  public getAllEmployes(): Observable<any> {
    //this.httpClient = new HttpClient(this.httpBackend);
    return this.httpClient.get<any>("/api/getallemployees", { responseType: "json" });
  }

  register(user: User) {
    console.log(user);
    return this.httpClient.post(`${environment.apiUrl}/NewMemberReg`, user);
  }


  delete(id: number) {
    return this.httpClient.get(`/users/${id}`);
  }

  adminDealStatusService() {
    return this.httpClient.get<any>(`${environment.apiUrl}/LoadDealReg`, { responseType: "json" });
  }

  approveOrRejectStatusService(statusDetail) {
    return this.httpClient.post(`${environment.apiUrl}/UpdateDealStatusID_ADO`,statusDetail);
  }
  approveOrRejectStatusServicePartner(statusDetail) {
    return this.httpClient.post(`${environment.apiUrl}/UpdateDealStatusIDPartner_ADO`,statusDetail);
  }
  GetDealRegInfo_ADO() {
    return this.httpClient.post(`${environment.apiUrl}/GetDealRegInfo_ADO`,{});
  }
  GetDealRegInfoPartner_ADO() {
    return this.httpClient.post(`${environment.apiUrl}/GetDealRegInfoPartner_ADO`,{});
  }
  GetDealRegInfoWithComments_ADO() {
    return this.httpClient.post(`${environment.apiUrl}/GetDealRegInfoWithComments_ADO`, {});
  }
  GetDealMappedSubscribers_ADO(params) {
    return this.httpClient.post(`${environment.apiUrl}/GetDealMappedSubscribers_ADO`, params);
  }
  AddOrRemoveSubscribers_ADO(params) {
    console.log(params);
    return this.httpClient.post(`${environment.apiUrl}/AddOrRemoveSubscribers_ADO`, params);
  }
  
  updateCommentService(dealDetails) {
    console.log("UpdateUserComments_ADO Params ", dealDetails);
    return this.httpClient.post(`${environment.apiUrl}/UpdateUserComments_ADO`, dealDetails);
  }

  generateReportService(generateReportDetails) {
    console.log(generateReportDetails);
    return this.httpClient.post(`${environment.apiUrl}/GetAdminReportData_ADO`, generateReportDetails);
  }

  loadStatusCountService(UserID) {
    console.log(UserID);
    return this.httpClient.post(`${environment.apiUrl}/GetDashboardData_ADO`, UserID);

  }
  
  loadCountService(UserId) {
    console.log(UserId);
    return this.httpClient.post(`${environment.apiUrl}/GetUserDashboard_ADO`, {UserId});
  }

  loadMyDealService(UserId) {
    console.log(UserId);
    return this.httpClient.post(`${environment.apiUrl}/UserDealFilterByID_ADO`, {UserId});
  }

  changePasswordService(changePasswordDetails) {
    console.log(changePasswordDetails);
    return this.httpClient.post(`${environment.apiUrl}/ChangePassword_ADO`, changePasswordDetails);
  }

  getAllDealStatusService() {
    return this.httpClient.get<any>(`${environment.apiUrl}/DealStatus`, { responseType: "json" });
  }

  resetPasswordService(emailID) {
    console.log(emailID);
    return this.httpClient.post(`${environment.apiUrl}/ForgotPassword_ADO`, {emailID});
  }

  getAllUsersService() {
    return this.httpClient.post(`${environment.apiUrl}/AdminGetAllUsers_ADO`, {});
  }
  
  adminUpdateUserService(editedDetails) {
    console.log(editedDetails);
    return this.httpClient.post(`${environment.apiUrl}/UpdateUserDetails_ADO`, editedDetails);
  }

  deleteUserService(userId) {
    console.log(userId);
    return this.httpClient.post(`${environment.apiUrl}/AdminDeleteUser_ADO`, {UserId:userId});
  }

  getSingleUserService(userId) {
    console.log(userId);
    return this.httpClient.post(`${environment.apiUrl}/userGetDetails_ADO`, {UserId:userId});
  }

  updateProfileService(editedDetails) {
    console.log(editedDetails);
    return this.httpClient.post(`${environment.apiUrl}/userSingleUpdate_ADO`, editedDetails);
  }

  getUserRegistrationDealService(userId) {
    console.log(userId);
    return this.httpClient.post(`${environment.apiUrl}/getUserRegistrationDeal_ADO`, {UserId:userId});
  }

  addCommentService(addedCommentDetails) {
    console.log(addedCommentDetails);
    return this.httpClient.post(`${environment.apiUrl}/userAddComment_ADO`, addedCommentDetails);
  }

  viewCommentsService(DealRegID) {
    console.log(DealRegID);
    return this.httpClient.post(`${environment.apiUrl}/adminViewUserComments_ADO`, {DealRegID:DealRegID});
  }

  
  
  

  // updateGeo(existingProject: Geo): Observable<Geo> {
  //   console.log(existingProject);
  //   return this.httpClient.put<Geo>(environment.apiUrl + `/UpdateGeo`, existingProject, { responseType: "json" });
  // }


  // roleMatch(allowedRoles): boolean {
  //     var isMatch = false;
  //     var userRoles: string[] = JSON.parse(localStorage.getItem('currentUser'));
  //     allowedRoles.forEach(element => {
  //       if (userRoles.indexOf(element) > -1) {
  //         isMatch = true;
  //         return false;
  //       }
  //     });
  //     return isMatch;
  //   }
}