import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from './../../environments/environment';
import { User, Role } from '../_models';

import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
     
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
       
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {

        var headers = new HttpHeaders();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        let urlSearchParams = new URLSearchParams();
        urlSearchParams.set('grant_type', 'password');
        urlSearchParams.set('username', username);
        urlSearchParams.set('password', password);
    
        var dummyUser: User;

        let body = urlSearchParams.toString();
     
        // return this.http.post<any>(`${environment.apiUrl}/users/authenticate`, { username, password })
        return this.http.post<any>( environment.apiTokenURL + '/token', body, {
            headers: headers
          })     .pipe(map(user => {
                    console.log(user);
                    //console.log(user.access_token);
                   
                    // login successful if there's a jwt token in the response
                    if (user && user.access_token) {
                       
                         dummyUser = {
                            id: user.id,
                            username: user.username,
                            firstName: user.firstName,
                            password: user.password,
                            lastName: user.lastName,
                            created_on: user.created_on,
                            role: user.role,
                            token: user.access_token
                        };
                        
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        // localStorage.setItem('currentUser', JSON.stringify(user));
                        localStorage.setItem('currentUser', JSON.stringify(dummyUser));
                        // this.currentUserSubject.next(user);
                        this.currentUserSubject.next(dummyUser);
                    }
    
                   // return user;
                   return dummyUser;
                }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}