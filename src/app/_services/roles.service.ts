import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Roles } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private httpClient: HttpClient) { }

   getAllRoles(): Observable<Roles[]> {
     return this.httpClient.get<Roles[]>(environment.apiUrl + `/roles`, { reportProgress: true, responseType: "json" })
       .pipe(map(
         (data: Roles[]) => {
             return data;
        }
       ));
  }

  getAllRolesService(roleDetails) 
  {
    console.log(roleDetails);
    return this.httpClient.post(environment.apiUrl + `/GetMasterTableRecords_ADO`, roleDetails);

  }

  insertProject(role: Roles) : Observable<Roles>
  {
    console.log(role);
    // var requestHeaders = new HttpHeaders();
    // requestHeaders = requestHeaders.set("X-XSRF-TOKEN", sessionStorage.XSRFRequestToken);
    // return this.httpClient.post<Project>("/api/projects", newProject, { headers: requestHeaders, responseType: "json" });
    debugger;
    return this.httpClient.post<Roles>(environment.apiUrl + `/newRole`, role);

  }

  updateProject(updatedRole: Roles) : Observable<Roles>
  {
    console.log(updatedRole);
    return this.httpClient.post<Roles>(environment.apiUrl + `/UpdateRole`, updatedRole, { responseType: "json" });
  }

  deleteProject(roleID: number) : Observable<string>
  {
    console.log(roleID);
    return this.httpClient.get<string>(environment.apiUrl +"/DeleteRole?ProjectID=" + roleID);
  }

  // SearchProjects(searchBy: string, searchText: string) : Observable<Project[]>
  // {
  //   return this.httpClient.get<Project[]>("/api/projects/search/" + searchBy + "/" + searchText, { responseType: "json" });
  // }

}
