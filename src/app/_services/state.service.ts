import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { State } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  constructor(private httpClient: HttpClient) { }

  getAllStateService(stateDetails) 
  {
    console.log(stateDetails);
    return this.httpClient.post(environment.apiUrl + `/GetMasterTableRecords_ADO`, stateDetails);

  }

  insert(state: State): Observable<State> {
    console.log(state);
    return this.httpClient.post<State>(environment.apiUrl + `/NewState`, state);

  }

  update(state: State): Observable<State> {
    console.log(state);
    return this.httpClient.post<State>(environment.apiUrl + `/UpdateState`, state, { responseType: "json" });
  }

  delete(ID: number): Observable<string> {
    debugger;
    return this.httpClient.get<string>(environment.apiUrl + "/DeleteState?StateID=" + ID);
  }
}
