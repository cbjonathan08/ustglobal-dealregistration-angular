import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Sla } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SlaService {

  constructor(private httpClient: HttpClient) { }

  getAllSlaService(slaDetails) 
  {
    return this.httpClient.post(environment.apiUrl + `/GetMasterTableRecords_ADO`, slaDetails);
  }

  insert(sla: Sla): Observable<Sla> {
    return this.httpClient.post<Sla>(environment.apiUrl + `/NewSla`, sla);

  }

  update(sla: Sla): Observable<Sla> {
    return this.httpClient.post<Sla>(environment.apiUrl + `/UpdateSla`, sla, { responseType: "json" });
  }

  delete(ID: number): Observable<string> {
    return this.httpClient.get<string>(environment.apiUrl + "/DeleteSla?SlaID=" + ID);
  }

  getSlaData(): Observable<Sla[]>{
      return this.httpClient.get<Sla[]>(environment.apiUrl+"/Sla",{ reportProgress: true, responseType: "json" })
      .pipe(map(
        (data: Sla[]) => {
          return data;
        }
      ));
  }

}
