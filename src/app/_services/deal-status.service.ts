import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DealStatus } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DealStatusService {

  constructor(private httpClient: HttpClient) { }

  getAllDealStatus(): Observable<DealStatus[]> {
    return this.httpClient.get<DealStatus[]>(environment.apiUrl + `/DealStatus`, { reportProgress: true, responseType: "json" })
      .pipe(map(
        (data: DealStatus[]) => {
          return data;
        }
      ));
  }

  getAllDealStatusService(dealStatusDetails) 
  {
    console.log(dealStatusDetails);
    return this.httpClient.post(environment.apiUrl + `/GetMasterTableRecords_ADO`, dealStatusDetails);

  }

  insert(role: DealStatus): Observable<DealStatus> {
    
    return this.httpClient.post<DealStatus>(environment.apiUrl + `/NewDealStatus`, role);

  }

  update(existingProject: DealStatus): Observable<DealStatus> {
    return this.httpClient.post<DealStatus>(environment.apiUrl + `/UpdateDealStatus`, existingProject, { responseType: "json" });
  }

  delete(ID: number): Observable<string> {
    
    return this.httpClient.get<string>(environment.apiUrl + "/DeleteDealStatus?DealStatusID=" + ID);
  }
}
