import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Observable } from 'rxjs';
import { Client, State, DealRegistration } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private httpClient: HttpClient) { }

  

  getAllClients(): Observable<Client[]> {
    return this.httpClient.get<Client[]>(environment.apiUrl + `/Clients`, { reportProgress: true, responseType: "json" })
      .pipe(map(
        (data: Client[]) => {
          return data;
        }
      ));
  }

  getAllClientService(clientDetails) 
  {
    console.log(clientDetails);
    return this.httpClient.post(environment.apiUrl + `/GetMasterTableRecords_ADO`, clientDetails);

  }

  getStates(countryId: number) {
    // return this.httpClient.get(`${environment.apiUrl}/states/${countryId}`).pipe(
    //   catchError(this.handleError)
    // );
   
    return this.httpClient.get<State[]>(environment.apiUrl + "/States?CountryID=" + countryId);
    // return this.httpClient.get(`${environment.apiUrl}/States/${countryId}`);
  }

  // private handleError(error: HttpErrorResponse) {
  //   if (error.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error('An error occurred:', error.error.message);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong,
  //     console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
  //   }
  //   // return an observable with a user-facing error message
  //   return throwError('Something bad happened. Please try again later.');
  // }s

  insert(role: Client): Observable<Client> {
    
    return this.httpClient.post<Client>(environment.apiUrl + `/NewClient`, role);

  }

  update(existingProject: Client): Observable<Client> {
    return this.httpClient.post<Client>(environment.apiUrl + `/UpdateClient`, existingProject, { responseType: "json" });
  }

  delete(ID: number): Observable<string> {
   
    return this.httpClient.get<string>(environment.apiUrl + "/DeleteClient?ClientID=" + ID);
  }

  dealregistration(dealregistration: DealRegistration) {
    console.log(dealregistration);
    return this.httpClient.post(`${environment.apiUrl}/NewDealReg`, dealregistration);
}

}
