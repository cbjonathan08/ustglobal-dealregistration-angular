import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Project } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private httpClient: HttpClient) { }

  getAllProjectService(projectDetails) 
  {
    console.log(projectDetails);
    return this.httpClient.post(environment.apiUrl + `/GetMasterTableRecords_ADO`, projectDetails);

  }

  getAllProjects(): Observable<Project[]> {
    return this.httpClient.get<Project[]>(environment.apiUrl + `/Projects`, { reportProgress: true, responseType: "json" })
      .pipe(map(
        (data: Project[]) => {
          return data;
        }
      ));
  }

  insert(role: Project): Observable<Project> {
    return this.httpClient.post<Project>(environment.apiUrl + `/NewProject`, role);

  }

  update(existingProject: Project): Observable<Project> {
    return this.httpClient.post<Project>(environment.apiUrl + `/UpdateProject`, existingProject, { responseType: "json" });
  }

  delete(ID: number): Observable<string> {
    return this.httpClient.get<string>(environment.apiUrl + "/DeleteProject?ProjectID=" + ID);
  }
}
