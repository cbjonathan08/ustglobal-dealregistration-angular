import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Geo } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeoService {

  constructor(private httpClient: HttpClient) { }

  getAllGeoService(roleDetails) 
  {
    console.log(roleDetails);
    return this.httpClient.post(environment.apiUrl + `/GetMasterTableRecords_ADO`, roleDetails);

  }

  getAllGeos(): Observable<Geo[]> {
    return this.httpClient.get<Geo[]>(environment.apiUrl + `/Geos`, { reportProgress: true, responseType: "json" })
      .pipe(map(
        (data: Geo[]) => {
          return data;
        }
      ));
  }


  insertGeo(role: Geo): Observable<Geo> {
    // var requestHeaders = new HttpHeaders();
    // requestHeaders = requestHeaders.set("X-XSRF-TOKEN", sessionStorage.XSRFRequestToken);
    // return this.httpClient.post<Project>("/api/projects", newProject, { headers: requestHeaders, responseType: "json" });
  
    return this.httpClient.post<Geo>(environment.apiUrl + `/NewGeo`, role);

  }

  updateGeo(existingProject: Geo): Observable<Geo> {
    console.log(existingProject);
    return this.httpClient.post<Geo>(environment.apiUrl + `/UpdateGeo`, existingProject, { responseType: "json" });
  }

  deleteGeo(ProjectID: number): Observable<string> {
   
    return this.httpClient.get<string>(environment.apiUrl + "/DeleteGeo?GeoID=" + ProjectID);
  }

  // SearchProjects(searchBy: string, searchText: string) : Observable<Project[]>
  // {
  //   return this.httpClient.get<Project[]>("/api/projects/search/" + searchBy + "/" + searchText, { responseType: "json" });
  // }

}
