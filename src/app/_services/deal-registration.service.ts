import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Observable } from 'rxjs';
import { DealRegistration } from '../_models';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DealRegistrationService {

  constructor(private httpClient: HttpClient) { }

  dealregistration(dealregistration: DealRegistration) {
    console.log(dealregistration);
    return this.httpClient.post(`${environment.apiUrl}/NewClient`, dealregistration);
}
}
