export class Project
{
    RoleID: number;
    RoleName: string;
    RoleDesc: string;
    CreatedBy: string;
    CreatedOn: string;
    ModifiedBy: string;
    ModifiedOn: string;

    constructor()
    {
        this.RoleID = 0;
        this.RoleName = null;
        this.RoleDesc = null;
        this.CreatedBy = null;
        this.CreatedOn = null;
        this.ModifiedBy = null;
        this.ModifiedOn = null;
    }
}