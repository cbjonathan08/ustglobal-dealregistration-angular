import { Component, OnDestroy, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '../_models';
import { UserService, AuthenticationService } from '../_services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  loading = false;
  currentUser: User;
  currentUserSubscription: Subscription;
  users: User[];

  constructor(
      private userService: UserService,
      private authenticationService: AuthenticationService
  ) {
      // this.currentUser = this.authenticationService.currentUserValue;

      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user;
    });
  }

  ngOnInit() {
      
      this.loadAllUsers();
     
      // // this.userService.getById(this.currentUser.id).pipe(first()).subscribe(user => {
      //   this.userService.getAll()
      //   .pipe(first())
      //   .subscribe(user => {
      //     this.users = user;
      // });
  }


  private loadAllUsers() {
   
    // this.userService.getAll().pipe(first()).subscribe(users => {
    //     this.users = users;
    // });
}

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.currentUserSubscription.unsubscribe();
}

}
