// import { Component, OnInit, OnDestroy } from '@angular/core';
// import { Subscription } from 'rxjs';
// import { AlertService } from '../../_services';

// @Component({
//   selector: 'app-alert',
//   templateUrl: './alert.component.html',
//   styleUrls: ['./alert.component.css']
// })
// export class AlertComponent implements OnInit, OnDestroy {

//   private subscription: Subscription;
//   message: any;
//   msginf: string;

//   constructor(private alertService: AlertService) {

//    }

//   ngOnInit() {
//     this.subscription = this.alertService.getAlert()
//     .subscribe(message => {
//         switch (message && message.type) {
//             case 'success':
//                // message.cssClass = 'alert alert-success';
//                 this.msginf = "Information";
//                 document.getElementById("btn_Error").click();
//                 break;
//             case 'error':
//                // message.cssClass = 'alert alert-danger';
//                 this.msginf = "Error";
//                 document.getElementById("btn_Error").click();
//                 break;
//         }

//         this.message = message;
//        // 
//     });
//   }

//   ngOnDestroy() {
//     this.subscription.unsubscribe();
//   }

// }

import { Component, OnInit, OnDestroy  } from '@angular/core';
import { Subscription } from 'rxjs';

import { AlertService } from './../../_services';

@Component({ selector: 'app-alert', templateUrl: 'alert.component.html' })
export class AlertComponent implements OnInit, OnDestroy  {
    private subscription: Subscription;
    message: any;

    constructor(private alertService: AlertService) { }

    // ngAfterViewInit() {
    //     alert('hii');
    //     this.myErrorText.nativeElement.focus();
    //   } this is commented code 

    ngOnInit() {
        //debugger;
        this.subscription = this.alertService.getAlert()
            .subscribe(message => {
                switch (message && message.type) {
                    case 'success':
                        message.cssClass = 'alert alert-success';
                        break;
                    case 'error':
                        message.cssClass = 'alert alert-danger';
                        break;
                }

                this.message = message;
            });
    }

    ngOnDestroy() {
        debugger;
        this.subscription.unsubscribe();
    }
}