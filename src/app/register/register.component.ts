import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, UserService, AuthenticationService } from '../_services';
// import { UserService, AuthenticationService } from '../_services';

// import custom validator to validate that password and confirm password fields match
import { MustMatch } from './../_helpers'
import { Roles } from '../_models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  roles: Roles[] = [];
  responseFromServer;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      //debugger;
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: [null, [Validators.required]],
      lastName: [null, Validators.required],
      username: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]],
      confirmPassword: [null, Validators.required],
      contactNumber: [null],
      memberType: [null, Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
  });

  this.userService.getRoles().subscribe((response) =>
    {
      //debugger;
      this.responseFromServer = response;
      let arr=this.responseFromServer;
      var arr2=[];
      for(var data of arr){
          if(!(data.roleID==1 || data.roleID==34)){
            arr2.push(data);
          }
      }
      this.roles=arr2;

      
    },
    error => {
      this.alertService.error(error);
      this.loading = false;
  }
    );

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    
    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }

    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));

    this.loading = true;
    this.userService.register(this.registerForm.value)
        .pipe(first())
        .subscribe(
            data => {
              //debugger;
                this.alertService.success('Registration successful', true);
                // this.error ="Registration successful";
                this.submitted = false;
                this.loading = false;
                this.registerForm.reset();
                // this.router.navigate(['/login']);
            },
            error => {
                
                this.alertService.error('Email already exist. Please register with different email.');
              
               // this.error = error;
                this.loading = false;
            });
}

onReset() {
  this.submitted = false;
  this.registerForm.reset();
}

}
