import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor, JwtInterceptor, LoaderInterceptorService } from './_helpers';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './footer/footer.component';
import { RegisterComponent } from './register/register.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RoleComponent } from './admin/role/role.component';
import { GeoComponent } from './admin/geo/geo.component';
import { MembertypeComponent } from './admin/membertype/membertype.component';
import { ForgotpasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AlertComponent } from './_components';
import { HomeComponent } from './home/home.component';
import { RolepermissionsComponent } from './admin/rolepermissions/rolepermissions.component';
import { CountryComponent } from './admin/country/country.component';
import { StateComponent } from './admin/state/state.component';
import { SlaComponent } from './admin/sla/sla.component';
import { TimelineComponent } from './admin/timeline/timeline.component';
import { DealstatusComponent } from './admin/dealstatus/dealstatus.component';
import { ClientComponent } from './admin/client/client.component';
import { MemberregComponent } from './admin/memberreg/memberreg.component';
import { NewdealregComponent } from './admin/newdealreg/newdealreg.component';
import { MemberdealstatusComponent } from './admin/memberdealstatus/memberdealstatus.component';
import { DealcommentsComponent } from './admin/dealcomments/dealcomments.component';
import { DealsubscribersComponent } from './admin/dealsubscribers/dealsubscribers.component';
import { AdmindashboardComponent } from './admin/admindashboard/admindashboard.component';
import { UserdashboardComponent } from './user/userdashboard/userdashboard.component';
import { UserdealregComponent } from './user/userdealreg/userdealreg.component';
import { UserdealregstatusComponent } from './user/userdealregstatus/userdealregstatus.component';
import { UserdealregcommentsComponent } from './user/userdealregcomments/userdealregcomments.component';
import { ManagepasswordComponent } from './admin/managepassword/managepassword.component';
import { AlertDirective } from './directives/alert.directive';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { LoaderComponent } from './_components/loader/loader.component';
import { ProjectComponent } from './admin/project/project.component';
import { FilterPipe } from './pipes/filter.pipe';
import { ArraySortPipe } from './pipes/sort.pipe';
import { ReportComponent } from './admin/report/report.component';
import { GenerateReportComponent } from './generate-report/generate-report.component';
import { UsermanageprofileComponent } from './user/usermanageprofile/usermanageprofile.component';
import { AdminmanageusersComponent } from './admin/adminmanageusers/adminmanageusers.component';
import { PartnerdealstatusComponent } from './partner/partnerdealstatus/partnerdealstatus.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    FooterComponent,
    RegisterComponent,
    PageNotFoundComponent,
    RoleComponent,
    GeoComponent,
    MembertypeComponent,
    ForgotpasswordComponent,
    ChangePasswordComponent,
    AlertComponent,
    HomeComponent,
    RolepermissionsComponent,
    CountryComponent,
    StateComponent,
    SlaComponent,
    TimelineComponent,
    DealstatusComponent,
    ClientComponent,
    MemberregComponent,
    NewdealregComponent,
    MemberdealstatusComponent,
    DealcommentsComponent,
    DealsubscribersComponent,
    AdmindashboardComponent,
    UserdashboardComponent,
    UserdealregComponent,
    UserdealregstatusComponent,
    UserdealregcommentsComponent,
    ManagepasswordComponent,
    AlertDirective,
    ContactComponent,
    AboutComponent,
    LoaderComponent,
    ProjectComponent,
    FilterPipe,
    ArraySortPipe,
    ReportComponent,
    GenerateReportComponent,
    UsermanageprofileComponent,
    AdminmanageusersComponent,
    PartnerdealstatusComponent

  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule,
    Ng2SearchPipeModule ,
    AutocompleteLibModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptorService, multi: true },
    // {provide : LocationStrategy , useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
