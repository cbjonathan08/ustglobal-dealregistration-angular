import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService, AuthenticationService } from './../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  currentRole;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }



  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }


  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        (response) => {
          let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
          console.log(currentUser);
        
          if(currentUser.role === 'Admin'){
            this.router.navigate(['admin/dashboard']);
          }
          if(currentUser.role=== 'Partner Admin'){
            this.router.navigate(['partner/dashboard' ]);
          }
          if(currentUser.role=== 'Member'){
            this.router.navigate(['user/userdashboard']);
          }
          
          // if (this.authenticationService.currentUserRole == "Admin")
          // {
          //   this.router.navigate(["/admin", "dashboard"]);
          // }
          // else
          // {
          //   this.router.navigate(["/employee", "tasks"]);
          // }
        },
        (error) => {
          console.log(error);
          this.alertService.error(error);
          this.error = error;
          this.loading = false;
        });
  }
  // onLoginClick(event)
  // {
  //   this.loginService.Login(this.loginViewModel).subscribe(
  //     (response) =>
  //     {
  //       if (this.loginService.currentUserRole == "Admin")
  //       {
  //         this.router.navigate(["/admin", "dashboard"]);
  //       }
  //       else
  //       {
  //         this.router.navigate(["/employee", "tasks"]);
  //       }
  //     },
  //     (error) =>
  //     {
  //       console.log(error);
  //       this.loginError = "Invalid Username or Password";
  //     },
  //   );
  // }
}
