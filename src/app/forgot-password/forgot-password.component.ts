import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService, UserService, AuthenticationService } from './../_services';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  Result: any;
  emailValue;
  resetPasswordRes;
  showMessage;
  fillRequiredValue;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private userService: UserService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.forgotPasswordForm.controls; }

  resetPassword() {
    if(this.emailValue === undefined){
      this.fillRequiredValue = 'Please fill in email'
      this.alertService.error( this.fillRequiredValue);
    }
      let emailID = this.emailValue;
      console.log(this.emailValue);
      this.userService.resetPasswordService(emailID).subscribe((response) =>
      {
        var resultString = JSON.parse(response[0].data);
        var resultStatus = JSON.parse(response[0].status);
        this.resetPasswordRes = resultString['Table'];
        console.log(this.resetPasswordRes);
        console.log(resultStatus);
        if(resultStatus === true){
          if(this.resetPasswordRes[0].Code === 20){
              this.showMessage = this.resetPasswordRes[0].descripion;
              this.alertService.error(this.showMessage);
          }
          if(this.resetPasswordRes[0].Code === 10){
            this.showMessage = this.resetPasswordRes[0].descripion;
            this.alertService.success(this.showMessage);
        }
        }
        else{
          this.alertService.error(this.showMessage);
        }
      },
      error => {
       console.log(error);
    }
      );
    
  }
}
