import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RoleComponent } from './admin/role/role.component';
import { SlaComponent  } from './admin/sla/sla.component';
import { ForgotpasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

import { AuthGuard } from './_helpers';
import { HomeComponent } from './home/home.component';
import { Role } from './_models';
import { RolepermissionsComponent } from './admin/rolepermissions/rolepermissions.component';
import { GeoComponent } from './admin/geo/geo.component';
import { CountryComponent } from './admin/country/country.component';
import { StateComponent } from './admin/state/state.component';
import { TimelineComponent } from './admin/timeline/timeline.component';
import { ClientComponent } from './admin/client/client.component';
import { DealstatusComponent } from './admin/dealstatus/dealstatus.component';
import { MemberregComponent } from './admin/memberreg/memberreg.component';
import { NewdealregComponent } from './admin/newdealreg/newdealreg.component';
import { MemberdealstatusComponent } from './admin/memberdealstatus/memberdealstatus.component';
import { DealcommentsComponent } from './admin/dealcomments/dealcomments.component';
import { DealsubscribersComponent } from './admin/dealsubscribers/dealsubscribers.component';
import { AdmindashboardComponent } from './admin/admindashboard/admindashboard.component';
import { UserdashboardComponent } from './user/userdashboard/userdashboard.component';
import { UserdealregComponent } from './user/userdealreg/userdealreg.component';
import { UserdealregstatusComponent } from './user/userdealregstatus/userdealregstatus.component';
import { UserdealregcommentsComponent } from './user/userdealregcomments/userdealregcomments.component';
import { UsermanageprofileComponent } from './user/usermanageprofile/usermanageprofile.component';
import { ManagepasswordComponent } from './admin/managepassword/managepassword.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { ProjectComponent } from './admin/project/project.component';
import { ReportComponent } from './admin/report/report.component';
import { AdminmanageusersComponent } from './admin/adminmanageusers/adminmanageusers.component';
import { PartnerdealstatusComponent } from './partner/partnerdealstatus/partnerdealstatus.component';


const appRoutes: Routes = [

  { path: '', canActivate: [AuthGuard], component: HomeComponent, pathMatch: 'full' },
  { path: 'contact', component: ContactComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'lostPassword', component: ForgotpasswordComponent },
  { path: 'changePassword',canActivate: [AuthGuard],  component: ChangePasswordComponent },

  { path: 'admin/dashboard', canActivate: [AuthGuard], data: { roles: ['Admin']}, component: AdmindashboardComponent },
  { path: 'admin/role', canActivate: [AuthGuard], data: { roles: ['Admin']},  component: RoleComponent },
  { path: 'admin/rolepermissions', component: RolepermissionsComponent },
  { path: 'admin/geo', canActivate: [AuthGuard], data: { roles: ['Admin']}, component: GeoComponent },
  { path: 'admin/country',canActivate: [AuthGuard], data: { roles: ['Admin']},  component: CountryComponent },
  { path: 'admin/state',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: StateComponent },
  { path: 'admin/sla',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: SlaComponent },

  { path: 'admin/timeline',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: TimelineComponent },
  { path: 'admin/client',canActivate: [AuthGuard], data: { roles: ['Admin']},component: ClientComponent },
  { path: 'admin/dealstatus',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: DealstatusComponent },
  { path: 'admin/project', canActivate: [AuthGuard], data: { roles: ['Admin']},component: ProjectComponent },
  { path: 'admin/project',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: ProjectComponent },
  { path: 'admin/report',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: ReportComponent },

  { path: 'admin/memberreg',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: MemberregComponent },
  { path: 'admin/newdealreg',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: UserdealregComponent },
  { path: 'admin/memberdealstatus',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: MemberdealstatusComponent },
  { path: 'admin/dealcomments',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: DealcommentsComponent },
  { path: 'admin/dealsubscribers',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: DealsubscribersComponent },
  { path: 'admin/managepassword',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: ManagepasswordComponent },
  { path: 'admin/adminmanageusers',canActivate: [AuthGuard], data: { roles: ['Admin']}, component: AdminmanageusersComponent },
  
  { path: 'user/userdashboard',canActivate: [AuthGuard], component: UserdashboardComponent },
  { path: 'user/userdealreg',canActivate: [AuthGuard], component: UserdealregComponent },
  { path: 'user/userdealregstatus',canActivate: [AuthGuard], component: UserdealregstatusComponent },
  { path: 'user/userdealregcomments',canActivate: [AuthGuard], component: UserdealregcommentsComponent },
  { path: 'user/usermanageprofile',canActivate: [AuthGuard], component: UsermanageprofileComponent },
  { path: 'partner/dashboard', canActivate: [AuthGuard], data: { roles: ['Partner Admin']}, component: AdmindashboardComponent },
  { path: 'partner/partnerdealstatus', canActivate: [AuthGuard], data: { roles: ['Partner Admin']}, component: PartnerdealstatusComponent },


  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes,{ useHash: true })], //, { useHash: true }
  exports: [RouterModule]
})
export class AppRoutingModule { }
